# AC Unreal

AC Unreal is a set of tools to export and import .dat data into Unreal Engine
.dat data is only used for import and is never cooked or packaged

* Exports fbx/t3d/wav/png/dxt/etc.
* Scripted import of assets and maps

### Installation

* 32GB of RAM is needed or you will need to restart the editor between steps
* Unreal 5.3
* Visual Studio 2022
* Clone this repo
* Right-click ACUnreal.uproject and generate project files
* Open in VS and Build

### Export and Import

* Open ACUnreal.uproject in Unreal Editor
* Disable Auto Save (or you will lock up your editor between steps if not run out of memory and crash)
* In Content Browser, find and right-click Editor/EditorWidget_ExportImport, click `Run Editor Utility Widget`
* RAM > 32GB
  * Click Run All
* RAM <= 32GB
  * Click Export Resources
  * Click Import Textures
  * Click Import Materials
  * Click Import Models
  * Restart to clear out used RAM if all import steps run in same session
* Create a new (Basic) map and save in a subfolder (i.e. ./Content/Maps/Dereth/Dereth.umap)
* Disable `Cast Dynamic Shadows` on the Actors `DirectionalLight` and `SkyLight` (mesh loading distance and no LODs are extremely high-cost)
* Open Window -> World Settings
  * Enable `World Composition` and disable `Enable World Bounds Checks`
  * Do not enable `Enable World Origin Rebasing` when importing or developing on the world (it can be used once the world is complete)
* Select the `PlayerStart` actor and move it to (X=800000.000000,Y=-1000000.000000,Z=9500.000000)
* Select the `Floor` actor and move it to (X=800000.000000,Y=-1000000.000000,Z=9399.000000)
* Open Window -> Levels -> Levels ▼
  * `Import tiled landscape...`
    * `Select Heightmap Tiles...` ./Saved/Landblocks/*.r16s (exclude Tile_All.r16)
    *  Disable `Flip Tile Y Coordinate`
    * `Tile Coordinates Offset` - `-4, -4`
    * `Import Configuration` - `Components: 17x17 Sections: 1x1 Quads: 15x15`
    * `Landscape Scale` - `2400 x 2400 x 12800`
    * `Material` - None
* Load all tiles (shift-select -> right-click -> `Load`)
* Open World Composition (right-most icon in Levels window)
* Create a new layer wtih desired streaming distance (i.e. 250000 for 2.5KM)
* Select all tiles and assign them to the new layer (right-click selected tiles in World Composition or Levels list)
* Click the Landscape Actor (not the proxies)
  * Change `LOD Distribution`; `LOD 0` to 10 and `Other LODs` to 10
* File -> Save All
* Select `LandscapeStreamingProxy` Actors of one column of Tiles (i.e. x0,y0-x0,y7)
  * From `EditorWidget_ExportImport` click `Apply Landscape Textures`
  * Wait for `Compiling Shaders` to complete
  * Save the tiles, unload them and repeat this process for each column
  * If you have a high end video card, you can do all tiles at once
  * Check RAM and restart if running low (save levels), repeat this process until complete
* Import Landblocks (repeat column process of `Apply Landscape Textures`)
  * Select all `LandscapeStreamingProxy` Actors
  * From `EditorWidget_ExportImport` click `Import Landblocks`
    * Imported landblocks and and scenes will create new levels
    * Select all same-tile levels ending with _Scene and _Landblocks in the levels window and drag/drop them to their tile
      * i.e. the level structure for x0, y0 should be as follows:
        * Tile_x0_y0
          * Tile_x0_y0_000_224_Scenes
          * Tile_x0_y0_000_240_Scenes
          * Tile_x0_y0_016_224_Scenes
          * Tile_x0_y0_016_240_Scenes
          * Tile_x0_y0_Landblocks
    * Check RAM and restart if running low (save levels), repeat this process until complete

*** Note: When saving levels save the persistent level with all tiles unloaded, or they will load when opening the map

### Todos

 - Convert hand-import steps to automated and automate entire process (make game button)
 - Alpha-blends on landscape textures
 - Convert scenes to foliage system
 - Replace temp road texture in landscape materials with spline roads
 - PBR/srgb correct materials
 - Animation
 - Audio
 - Composition options for generated .t3d hierarchies (actors/components/instances/etc.)
 - Composition options for mapping landblocks to tiles by size
 - Automated map creation and hierarchical landscape tile import
 - Async export/import (no blocking steps)
 - Support 0x05 mip chains
 - Support 0x01 lods
 - Landscape holes
 - Landscape triangulation matches AC (engine modification?)
 - Portal drop locations
 - Dat versioning/support old formats
 - Versioned maps/assets from dats by version
 - Environment (0x0Ds) streaming for discrete dungeon loading/unloading
 - Skybox/dome imports
 - Weather
 - Font and string imports (with UE localization)
 - Import scaling and rotating options (i.e. keep original AC scale and transform)
 - Utility features - asset skinning/ACE&GDLE client adaptor/split-screen for multibox
 - Game simulation (game code/items/movement/combat/skills/attributes/buffs/etc.) [retirement plan]

License
----

MIT
