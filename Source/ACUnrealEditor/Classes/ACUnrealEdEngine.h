#pragma once

#include "ACUnrealEdEngine.generated.h"

DEFINE_LOG_CATEGORY_STATIC(LogACUnreal_Editor, Log, All);

struct FACUnrealEdImportPayload
{
	FString FileName;

	FString Payload;
};

UCLASS()
class UACUnrealEdEngine : public UUnrealEdEngine
{
	GENERATED_UCLASS_BODY()

	void ImportT3D(const FString& FileName, const FString& T3DPayLoad, bool FocusImportedActor, const FString& LevelNameToActivate);

	void ImportT3DToBlueprint(const FString& FileName, const FString& T3DPayLoad, const FName AssetName);

	void ClearImportQueue();

	void ClearFocusQueue();

	void StartFocusImportActors();

	bool LoadAndMakeLevelStreamingActive(ULevelStreaming* LevelStreaming);

	virtual void Tick(float DeltaSeconds, bool bIdleMode) override;

private:
	typedef TMap<FName, FACUnrealEdImportPayload> TImportToBlueprintMap;
	TImportToBlueprintMap T3DImportToBlueprintQueue;
	typedef TMap<FString, TArray<FACUnrealEdImportPayload>> TImportMap;
	TImportMap T3DImportQueue;
	TArray<AActor*> FocusQueue;
	float FocusTime;
	bool FocusImportedActors;
};
