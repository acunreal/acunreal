#include "ACUnrealEditor.h"
#include "ACUnrealDatFile.h"
#include "ACUnrealEdEngine.h"
#include "ACUnrealMaterialImporter.h"
#include "AssetToolsModule.h"
#include "BodySetupEnums.h"
#include "Developer/DesktopPlatform/Public/IDesktopPlatform.h"
#include "Developer/DesktopPlatform/Public/DesktopPlatformModule.h"
#include "HAL/PlatformApplicationMisc.h"
#include "InterchangeManager.h"
#include "LandscapeComponent.h"
#include "LandscapeInfo.h"
#include "LandscapeInfoMap.h"
#include "LandscapeStreamingProxy.h"
#include "Misc/ScopedSlowTask.h"
#include "UnrealEd.h"

#define LOCTEXT_NAMESPACE "ACUnrealEditorFunctionLibrary"

static void SaveContent()
{
	const bool bPromptUserToSave = false;
	const bool bSaveMapPackages = false;
	const bool bSaveContentPackages = true;
	const bool bFastSave = false;
	const bool bNotifyNoPackagesSaved = false;
	const bool bCanBeDeclined = true;
	FEditorFileUtils::SaveDirtyPackages(bPromptUserToSave, bSaveMapPackages, bSaveContentPackages, bFastSave, bNotifyNoPackagesSaved, bCanBeDeclined);
}

int32 UACUnrealEditorFunctionLibrary::HexStringToInt32(const FString& HexString)
{
	return FCString::Strtoi(*HexString, nullptr, 16);
}

bool UACUnrealEditorFunctionLibrary::OpenDirectoryDialog(const FString& DialogTitle, const FString& DefaultPath, FString& OutFolderName)
{
	if (IDesktopPlatform* DesktopPlatform = FDesktopPlatformModule::Get())
	{
		const void* ParentWindowHandle = FSlateApplication::Get().FindBestParentWindowHandleForDialogs(nullptr);
		return DesktopPlatform->OpenDirectoryDialog(ParentWindowHandle, DialogTitle, DefaultPath, OutFolderName);
	}

	return false;
}

void UACUnrealEditorFunctionLibrary::GetDatFileDirectory(FString& OutPath, bool& OutHasCell, bool& OutHasHighRes, bool& OutHasLocale, bool& OutHasPortal)
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		ACUnrealDatToolsModule->GetDatFileDirectory(OutPath, OutHasCell, OutHasHighRes, OutHasLocale, OutHasPortal);
	}
}

void UACUnrealEditorFunctionLibrary::SetDatFileDirectory(const FString& Path)
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		ACUnrealDatToolsModule->SetDatFileDirectory(Path);
	}
}

void UACUnrealEditorFunctionLibrary::ExportLandblocks()
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		ACUnrealDatToolsModule->ExportLandblocks();
	}
}

void UACUnrealEditorFunctionLibrary::ExportPortalResource(EACUnrealResource::Type ResourceType, int32 Identifier)
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		ACUnrealDatToolsModule->ExportResource(ResourceType, Identifier);
	}
}

void UACUnrealEditorFunctionLibrary::ExportPortalResources(EACUnrealResource::Type ResourceType, int32 Mask)
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		ACUnrealDatToolsModule->ExportResources(ResourceType, Mask);
	}
}

void UACUnrealEditorFunctionLibrary::DumpDatFileStatsToLog(EACUnrealDatFile::Type DatFileType)
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		ACUnrealDatToolsModule->DumpDatFileStatsToLog(DatFileType, true);
	}
}

void UACUnrealEditorFunctionLibrary::ImportTextures(const FString& SourceFolder, const FString& TargetSubPath)
{
	if (FPaths::DirectoryExists(SourceFolder))
	{
		TArray<FString> TextureFiles;
		{
			IFileManager& FileManager = IFileManager::Get();
			FileManager.FindFiles(TextureFiles, *(SourceFolder / "*.dxt"), true, false);
			FileManager.FindFiles(TextureFiles, *(SourceFolder / "*.png"), true, false);

			const FString FilenamePrefix = (SourceFolder.EndsWith(TEXT("/")) || SourceFolder.EndsWith(TEXT("\\"))) ? SourceFolder : SourceFolder + TEXT("/");
			for (FString& FileName : TextureFiles)
			{
				FileName.InsertAt(0, *FilenamePrefix);
			}
		}

		FAssetToolsModule& AssetToolsModule = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools");

		TArray<UObject*> ImportedAssets = AssetToolsModule.Get().ImportAssets(TextureFiles, TargetSubPath, nullptr, false, nullptr, true, false);

		if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
		{
			const int32 WorkItems_ImportCorrectTextureSettings = ImportedAssets.Num();
			FScopedSlowTask SlowTask_ImportCorrectTextureSettings(static_cast<float>(WorkItems_ImportCorrectTextureSettings), LOCTEXT("ImportCorrectTextureSettings", "Correcting Texture Settings"));
			SlowTask_ImportCorrectTextureSettings.MakeDialog();

			for (UObject* ImportedAsset : ImportedAssets)
			{
				SlowTask_ImportCorrectTextureSettings.EnterProgressFrame();

				if (UTexture2D * ImportedTexture = Cast<UTexture2D>(ImportedAsset))
				{
					ACUnrealDatToolsModule->CorrectPortalImageTextureSettings(ImportedTexture);
					ImportedAsset->MarkPackageDirty();
				}
			}
		}
	}
}

void UACUnrealEditorFunctionLibrary::ImportModels(const FString& SourceFolder, const FString& TargetSubPath)
{
	if (FPaths::DirectoryExists(SourceFolder))
	{
		UFbxFactory* FbxFactory = NewObject<UFbxFactory>(UFbxFactory::StaticClass(), FName("Factory"), RF_NoFlags);
		FbxFactory->ResetState();
		FbxFactory->SetDetectImportTypeOnImport(false);
		FbxFactory->ImportUI->bImportMaterials = true;
		FbxFactory->ImportUI->bImportTextures = true;
		FbxFactory->ImportUI->bImportAsSkeletal = false;
		FbxFactory->ImportUI->bImportMesh = true;
		FbxFactory->ImportUI->StaticMeshImportData->bAutoGenerateCollision = false;
		FbxFactory->ImportUI->StaticMeshImportData->bOneConvexHullPerUCX = false;
		FbxFactory->ImportUI->StaticMeshImportData->bCombineMeshes = true;
		FbxFactory->ImportUI->TextureImportData->MaterialSearchLocation = EMaterialSearchLocation::AllAssets;
		FbxFactory->ImportUI->SetMeshTypeToImport();

		TArray<FString> ModelFiles;
		{
			IFileManager& FileManager = IFileManager::Get();
			FileManager.FindFiles(ModelFiles, *(SourceFolder / "*.fbx"), true, false);

			const FString FilenamePrefix = (SourceFolder.EndsWith(TEXT("/")) || SourceFolder.EndsWith(TEXT("\\"))) ? SourceFolder : SourceFolder + TEXT("/");
			for (FString& FileName : ModelFiles)
			{
				FileName.InsertAt(0, *FilenamePrefix);
			}
		}

		FAssetToolsModule& AssetToolsModule = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools");

		UAutomatedAssetImportData* ImportData = NewObject<UAutomatedAssetImportData>();
		ImportData->GroupName = TargetSubPath; // just a display name
		ImportData->Filenames = ModelFiles;
		ImportData->DestinationPath = TargetSubPath;
		ImportData->FactoryName = FbxFactory->GetName();
		ImportData->bReplaceExisting = true;
		ImportData->bSkipReadOnly = false;
		ImportData->Factory = FbxFactory;

		TArray<UObject*> ImportedAssets = AssetToolsModule.Get().ImportAssetsAutomated(ImportData);

		if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
		{
			const int32 WorkItems_ImportModels = ImportedAssets.Num();
			FScopedSlowTask SlowTask_ImportModels(static_cast<float>(WorkItems_ImportModels), LOCTEXT("ImportModels", "Importing Models"));
			SlowTask_ImportModels.MakeDialog();

			for (UObject* ImportedAsset : ImportedAssets)
			{
				SlowTask_ImportModels.EnterProgressFrame();

				if (UStaticMesh* ImportedModel = Cast<UStaticMesh>(ImportedAsset))
				{
					if (ImportedModel->GetBodySetup())
					{
						if (ImportedModel->GetName().Contains(TEXT("0x0D"), ESearchCase::IgnoreCase))
						{
							ImportedModel->GetBodySetup()->CollisionTraceFlag = ECollisionTraceFlag::CTF_UseComplexAsSimple;
						}
					}

					ImportedAsset->MarkPackageDirty();
				}
			}
		}

		SaveContent();
	}
}

bool UACUnrealEditorFunctionLibrary::CorrectPortalImageTextureSettings(UTexture2D* Texture, int32 Identifier)
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		return ACUnrealDatToolsModule->CorrectPortalImageTextureSettings(Texture, Identifier);
	}

	return false;
}

void UACUnrealEditorFunctionLibrary::ImportScene(int32 Identifier)
{
	const FString FileName = FString::Printf(TEXT("0x%08X"), Identifier);
	const FString FileDir = FPaths::Combine(*FPaths::ProjectSavedDir(), TEXT("Scenes"));
	const FString FilePath = FString::Printf(TEXT("%s/%s.t3d"), *FileDir, *FileName);

	ImportT3D(FilePath, TEXT(""), false); // todo: empty level name here is wrong
}

void UACUnrealEditorFunctionLibrary::ImportSetup(int32 Identifier)
{
	if (0 == (0xFF000000 & Identifier))
	{
		Identifier |= EACUnrealResource_Native::kSetup;
	}

	const FString FileName = FString::Printf(TEXT("0x%08X"), Identifier);
	const FString FileDir = FPaths::Combine(*FPaths::ProjectSavedDir(), TEXT("Setups"));
	const FString FilePath = FString::Printf(TEXT("%s/%s.t3d"), *FileDir, *FileName);
	const FString AssetPath = FString::Printf(TEXT("/Game/Setups/%s"), *FileName);

	ImportT3DToBlueprint(FilePath, FName(AssetPath));
}

void UACUnrealEditorFunctionLibrary::ImportSetups(int32 Mask)
{
	const int32 FirstIdentifier = EACUnrealResource_Native::kSetup;
	const int32 LastIdentifer = FirstIdentifier | (0x0000FFFF & Mask);

	//const int32 WorkItems_ImportSurfaces = LastIdentifer - FirstIdentifier;
	//FScopedSlowTask SlowTask_ImportSurfaces(static_cast<float>(WorkItems_ImportSurfaces), LOCTEXT("ImportSurfaces", "Import Surface Materials"));
	//SlowTask_ImportSurfaces.MakeDialog();

	for (int32 Identifier = FirstIdentifier; Identifier < LastIdentifer; ++Identifier)
	{
		ImportSetup(Identifier);
	}
}

bool UACUnrealEditorFunctionLibrary::ImportSurface(int32 SurfaceId, UMaterial* DiffuseParentMat, UMaterial* DiffuseMaskedParentMat, FName DiffuseParamName, UMaterial* PalettedParentMat, UMaterial* PalettedMaskedParentMat, FName IndexedParamName, FName PaletteParamName)
{
	return FACUnrealMaterialImporter::ImportSurfaceToMaterial(SurfaceId, DiffuseParentMat, DiffuseMaskedParentMat, DiffuseParamName, PalettedParentMat, PalettedMaskedParentMat, IndexedParamName, PaletteParamName);
}

void UACUnrealEditorFunctionLibrary::ImportSurfaces(int32 Mask, UMaterial* DiffuseParentMat, UMaterial* DiffuseMaskedParentMat, FName DiffuseParamName, UMaterial* PalettedParentMat, UMaterial* PalettedMaskedParentMat, FName IndexedParamName, FName PaletteParamName)
{
	const int32 FirstIdentifier = EACUnrealResource_Native::kSurface;
	const int32 LastIdentifer = FirstIdentifier | (0x0000FFFF & Mask);

	const int32 WorkItems_ImportSurfaces = LastIdentifer - FirstIdentifier;
	FScopedSlowTask SlowTask_ImportSurfaces(static_cast<float>(WorkItems_ImportSurfaces), LOCTEXT("ImportSurfaces", "Import Surface Materials"));
	SlowTask_ImportSurfaces.MakeDialog();

	for (int32 Identifier = FirstIdentifier; Identifier < LastIdentifer; ++Identifier)
	{
		SlowTask_ImportSurfaces.EnterProgressFrame();

		ImportSurface(Identifier, DiffuseParentMat, DiffuseMaskedParentMat, DiffuseParamName, PalettedParentMat, PalettedMaskedParentMat, IndexedParamName, PaletteParamName);
	}
}

bool UACUnrealEditorFunctionLibrary::ImportLandscapeMaterials(UMaterialInterface* LandscapeParentMat)
{
	return FACUnrealMaterialImporter::ImportLandscapeMaterials(LandscapeParentMat);
}

bool UACUnrealEditorFunctionLibrary::ImportLandscapeMaterialInstances(UMaterialInterface* LandscapeParentMat)
{
	for (int32 TileY = 0; TileY < 8; ++TileY)
	{
		for (int32 TileX = 0; TileX < 8; ++TileX)
		{
			TArray<int32> TileGCFs = GetLandscapeTileGCFs(TileX, TileY);
			TSet<int32> TileUniqueGCFs;
			TileUniqueGCFs.Append(TileGCFs);

			const int32 WorkItems_ImportLandscapeMaterialInstances = TileUniqueGCFs.Num();
			FText ImportLandscapeMaterialInstancesText = FText::Format(LOCTEXT("ExportResourcesType", "Import Landscape Material Instances {0}, {1}"), TileX, TileY);
			FScopedSlowTask SlowTask_ImportLandscapeMaterialInstances(static_cast<float>(WorkItems_ImportLandscapeMaterialInstances), ImportLandscapeMaterialInstancesText);
			SlowTask_ImportLandscapeMaterialInstances.MakeDialog();

			for (int32 UniqueGCF : TileUniqueGCFs.Array())
			{
				SlowTask_ImportLandscapeMaterialInstances.EnterProgressFrame();

				FACUnrealMaterialImporter::ImportLandscapeMaterialInstances(FString::Printf(TEXT("LandscapeGCF_%d"), UniqueGCF), TileX, TileY);
			}
		}
	}

	return true;
}

void UACUnrealEditorFunctionLibrary::ExportLandblockInfo(int32 LandblockX, int32 LandblockY)
{
	if (IACUnrealDatToolsModule* ACUnrealDatToolsModule = FModuleManager::LoadModulePtr<IACUnrealDatToolsModule>("ACUnrealDatTools"))
	{
		ACUnrealDatToolsModule->ExportLandblockInfo(LandblockX, LandblockY);

		static volatile bool bImport = true;
		if (bImport)
		{
			ImportLandblockInfo(LandblockX, LandblockY, false);
		}
	}
}

void UACUnrealEditorFunctionLibrary::ImportT3D(const FString& FilePath, const FString& LevelName, bool FocusImportedActor)
{
	FString FileContent;
	if (FFileHelper::LoadFileToString(FileContent, *FilePath))
	{
		UACUnrealEdEngine* ACUnrealEd = Cast<UACUnrealEdEngine>(GEditor);
		if (ACUnrealEd)
		{
			ACUnrealEd->ImportT3D(FilePath, FileContent, FocusImportedActor, LevelName);
		}
		else // fallback to immediate paste-import
		{
			FPlatformApplicationMisc::ClipboardCopy(*FileContent);
			if (GEditor && GEditor->GetEditorWorldContext().World())
			{
				GEditor->Exec(GEditor->GetEditorWorldContext().World(), TEXT("EDIT PASTE"));
			}
		}
	}
}

void UACUnrealEditorFunctionLibrary::ImportT3DToBlueprint(const FString& FilePath, const FName AssetName)
{
	FString FileContent;
	if (FFileHelper::LoadFileToString(FileContent, *FilePath))
	{
		UACUnrealEdEngine* ACUnrealEd = Cast<UACUnrealEdEngine>(GEditor);
		if (ACUnrealEd)
		{
			ACUnrealEd->ImportT3DToBlueprint(FilePath, FileContent, AssetName);
		}
		else // fallback to immediate paste-import
		{
			FPlatformApplicationMisc::ClipboardCopy(*FileContent);
			if (GEditor && GEditor->GetEditorWorldContext().World())
			{
				GEditor->Exec(GEditor->GetEditorWorldContext().World(), TEXT("EDIT PASTE"));

				// todo: select current actor and call FKismetEditorUtilities::CreateBlueprint
			}
		}
	}
}

void UACUnrealEditorFunctionLibrary::ImportLandblockInfo(int32 LandblockX, int32 LandblockY, bool FocusImportedActor)
{
	const int32 UnrealTileX = LandblockX / 32;
	const int32 UnrealTileY = 7 - (LandblockY / 32);
	const FString LevelName = FString::Printf(TEXT("Tile_x%d_y%d_Landblocks"), UnrealTileX, UnrealTileY);

	const FString FileName = FString::Printf(TEXT("%03d_%03d_%02X%02XFFFE"), LandblockX, LandblockY, LandblockX, LandblockY);
	const FString FileDir = FPaths::Combine(*FPaths::ProjectSavedDir(), TEXT("LandblockInfos"));
	const FString FilePath = FString::Printf(TEXT("%s/%s.t3d"), *FileDir, *FileName);

	ImportT3D(FilePath, LevelName, FocusImportedActor);
}

void UACUnrealEditorFunctionLibrary::ImportLandblockScene(int32 LandblockX, int32 LandblockY, bool FocusImportedActor)
{
	const int32 UnrealTileX = LandblockX / 32;
	const int32 UnrealTileY = 7 - (LandblockY / 32);
	const FString LevelName = FString::Printf(TEXT("Tile_x%d_y%d_%03d_%03d_Scenes"), UnrealTileX, UnrealTileY, LandblockX, LandblockY);

	const FString FileName = FString::Printf(TEXT("%03d_%03d_%03d_%03d_Scenes"), LandblockX, LandblockY, LandblockX + kSceneLandblockWidth, LandblockY + kSceneLandblockWidth);
	const FString FileDir = FPaths::Combine(*FPaths::ProjectSavedDir(), TEXT("LandblockInfos"));
	const FString FilePath = FString::Printf(TEXT("%s/%s.t3d"), *FileDir, *FileName);

	ImportT3D(FilePath, LevelName, FocusImportedActor);
}

void UACUnrealEditorFunctionLibrary::MarkLandscapeComponentDirty(ULandscapeComponent* LandscapeComponent)
{
	if (LandscapeComponent)
	{
		LandscapeComponent->MarkPackageDirty();
	}
}

void UACUnrealEditorFunctionLibrary::UpdateLandscapeStreamingLayerInfoMap(ALandscapeStreamingProxy* LandscapeProxy)
{
	if (LandscapeProxy)
	{
		ULandscapeInfo* LandscapeInfo = nullptr;

		check(GIsEditor);
		UWorld* OwningWorld = LandscapeProxy->GetWorld();

		if (OwningWorld != nullptr && !OwningWorld->IsGameWorld())
		{
			auto& LandscapeInfoMap = ULandscapeInfoMap::GetLandscapeInfoMap(OwningWorld);
			LandscapeInfo = LandscapeInfoMap.Map.FindRef(LandscapeProxy->GetLandscapeGuid());
		}

		if (LandscapeInfo)
		{
			LandscapeInfo->UpdateLayerInfoMap(LandscapeProxy, true);
		}
	}
}

void UACUnrealEditorFunctionLibrary::StopImports()
{
	UACUnrealEdEngine* ACUnrealEd = Cast<UACUnrealEdEngine>(GEditor);
	if (ACUnrealEd)
	{
		ACUnrealEd->ClearImportQueue();
	}
}

void UACUnrealEditorFunctionLibrary::StopImportFocus()
{
	UACUnrealEdEngine* ACUnrealEd = Cast<UACUnrealEdEngine>(GEditor);
	if (ACUnrealEd)
	{
		ACUnrealEd->ClearFocusQueue();
	}
}

void UACUnrealEditorFunctionLibrary::StartImportFocus()
{
	UACUnrealEdEngine* ACUnrealEd = Cast<UACUnrealEdEngine>(GEditor);
	if (ACUnrealEd)
	{
		ACUnrealEd->StartFocusImportActors();
	}
}

TArray<int32> UACUnrealEditorFunctionLibrary::GetLandscapeTileGCFs(int32 TileX, int32 TileY)
{
	TArray<int32> TileGCFs;

	const FString FileDir = FPaths::Combine(*FPaths::ProjectSavedDir(), TEXT("/Landblocks/"));
	const FString FilePath = FString::Printf(TEXT("%s/Tile_x%d_y%d.gcfs"), *FileDir, TileX, TileY);

	FString FileContent;
	if (FFileHelper::LoadFileToString(FileContent, *FilePath))
	{
		TArray<FString> TerrainStringArray;
		FileContent.ParseIntoArray(TerrainStringArray, TEXT(","));
		for (const FString& TerrainString : TerrainStringArray)
		{
			TileGCFs.Add(static_cast<int32>(FCString::Atoi(*TerrainString)));
		}
	}

	return TileGCFs;
}

void UACUnrealEditorFunctionLibrary::MoveViewportCamerasToActors(const TArray<AActor*>& Actors, bool bActiveViewportOnly)
{
	if (GEditor)
	{
		GEditor->MoveViewportCamerasToActor(Actors, bActiveViewportOnly);
	}
}

bool UACUnrealEditorFunctionLibrary::IsAsyncImportOrExportPending()
{
	return UInterchangeManager::GetInterchangeManager().IsInterchangeActive();
}

void UACUnrealEditorFunctionLibrary::SetMinDrawDistance(UPrimitiveComponent* Component, float Distance)
{
	if (Component)
	{
		Component->MinDrawDistance = Distance;
	}
}

#undef LOCTEXT_NAMESPACE
