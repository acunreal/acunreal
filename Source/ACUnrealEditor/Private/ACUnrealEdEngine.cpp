#include "ACUnrealEditor.h"
#include "Engine/World.h"
#include "Engine/WorldComposition.h"
#include "EditorLevelUtils.h"
#include "EditorLevelLibrary.h"
#include "HAL/PlatformApplicationMisc.h"
#include "Kismet2/KismetEditorUtilities.h"
#include "LevelUtils.h"

UACUnrealEdEngine::UACUnrealEdEngine(const class FObjectInitializer& PCIP)
: Super(PCIP)
, FocusTime(0.0f)
, FocusImportedActors(false)
{
}

void UACUnrealEdEngine::ImportT3D(const FString& FileName, const FString& T3DPayLoad, bool FocusImportedActor, const FString& LevelNameToActivate)
{
	const int32 AtIndex = T3DImportQueue.FindOrAdd(LevelNameToActivate).Insert({ FileName, T3DPayLoad }, 0);

	if (0 == AtIndex)
	{
		T3DImportQueue.KeySort(TLess<FString>());
	}

	FocusImportedActors = FocusImportedActor;
}

void UACUnrealEdEngine::ImportT3DToBlueprint(const FString& FileName, const FString& T3DPayLoad, const FName AssetName)
{
	T3DImportToBlueprintQueue.Add(AssetName, { FileName, T3DPayLoad });
}

void UACUnrealEdEngine::ClearImportQueue()
{
	T3DImportQueue.Empty();
}

void UACUnrealEdEngine::ClearFocusQueue()
{
	FocusQueue.Empty();
	FocusImportedActors = false;
}

void UACUnrealEdEngine::StartFocusImportActors()
{
	FocusImportedActors = true;
}

bool UACUnrealEdEngine::LoadAndMakeLevelStreamingActive(ULevelStreaming* LevelStreaming)
{
	UWorld* const EditorWorldFromContext = GetEditorWorldContext().World();

	if (!EditorWorldFromContext || !LevelStreaming)
	{
		return false;
	}

	ULevelStreamingDynamic* AsKismetLevelStreaming = Cast<ULevelStreamingDynamic>(LevelStreaming);
	if (AsKismetLevelStreaming)
	{
		if (!AsKismetLevelStreaming->IsLevelLoaded() || !AsKismetLevelStreaming->bInitiallyLoaded || !AsKismetLevelStreaming->bInitiallyVisible || !AsKismetLevelStreaming->bShouldBlockOnLoad)
		{
			AsKismetLevelStreaming->SetShouldBeLoaded(true);
			AsKismetLevelStreaming->SetShouldBeVisible(true);
			AsKismetLevelStreaming->bShouldBlockOnLoad = true;
			AsKismetLevelStreaming->bInitiallyLoaded = true;
			AsKismetLevelStreaming->bInitiallyVisible = true;
		}

		if (!EditorWorldFromContext->GetStreamingLevels().Contains(LevelStreaming))
		{
			EditorWorldFromContext->AddStreamingLevel(AsKismetLevelStreaming);
		}

		if (!EditorWorldFromContext->WorldComposition->TilesStreaming.Contains(LevelStreaming))
		{
			EditorWorldFromContext->WorldComposition->TilesStreaming.Add(LevelStreaming);
		}

		EditorWorldFromContext->FlushLevelStreaming();

		AsKismetLevelStreaming->bShouldBlockOnLoad = false;

		UEditorLevelUtils::MakeLevelCurrent(LevelStreaming);

		if (LevelStreaming->GetLoadedLevel() && EditorWorldFromContext->GetCurrentLevel() != LevelStreaming->GetLoadedLevel())
		{
			return EditorWorldFromContext->SetCurrentLevel(LevelStreaming->GetLoadedLevel());
		}
	}

	return EditorWorldFromContext->GetCurrentLevel() == LevelStreaming->GetLoadedLevel();
}

class FDelegateBaseAccessHack : public TDelegateBase<FThreadSafeDelegateMode>
{
public:
	// avoids calling FDelegateBase::GetDelegateInstance() which is deprecated
	const IDelegateInstance* GetDelegateInstanceProtectedHack() const
	{
		return GetDelegateInstanceProtected();
	}
};

void UACUnrealEdEngine::Tick(float DeltaSeconds, bool bIdleMode)
{
	Super::Tick(DeltaSeconds, bIdleMode);

	UWorld* const EditorWorldFromContext = GetEditorWorldContext().World();

	auto T3DImportToBlueprintIter = T3DImportToBlueprintQueue.CreateIterator();
	if (T3DImportToBlueprintIter)
	{
		TArray<FName> Imported;
		AActor* ImportedActor = nullptr;
		UBlueprint* NewBlueprint = nullptr;

		while (T3DImportToBlueprintIter)
		{
			const FACUnrealEdImportPayload& ImportPayload = T3DImportToBlueprintIter.Value();

			FPlatformApplicationMisc::ClipboardCopy(*ImportPayload.Payload);

			FString PastedData;
			FPlatformApplicationMisc::ClipboardPaste(PastedData);

			if (PastedData.Len() > 0)
			{
				UE_LOG_ACUNREALED(Log, TEXT("ImportToBlueprint %s"), *ImportPayload.FileName);

				Exec(GetEditorWorldContext().World(), TEXT("EDIT PASTE"));

				const FName ImportedAssetName = T3DImportToBlueprintIter.Key();
				const FString ImportedAssetNameString = ImportedAssetName.ToString();

				if (GetSelectedActors())
				{
					if (AActor* SelectedActor = Cast<AActor>(GetSelectedActors()->GetSelectedObject(0)))
					{
						ImportedActor = SelectedActor;
					}
				}

				if (!ImportedActor)
				{
					for (TActorIterator<AActor> It(GetEditorWorldContext().World()); It; ++It)
					{
						if (AActor* Actor = *It)
						{
							if (ImportedAssetNameString.Contains(Actor->GetName()))
							{
								ImportedActor = Actor;
								break;
							}
						}
					}
				}

				if (ImportedActor)
				{
					FKismetEditorUtilities::FCreateBlueprintFromActorParams CreateParams;
					CreateParams.bDeferCompilation = false;
					CreateParams.bKeepMobility = false;
					CreateParams.bOpenBlueprint = false;
					CreateParams.bReplaceActor = false;
					NewBlueprint = FKismetEditorUtilities::CreateBlueprintFromActor(ImportedAssetNameString, ImportedActor, CreateParams);

					Imported.Add(ImportedAssetName);

					break; // todo: should skip other queue?
				}
			}
			else
			{
				UE_LOG_ACUNREALED(Warning, TEXT("ImportToBlueprint %s failed, will retry"), *ImportPayload.FileName);
			}

			++T3DImportToBlueprintIter;
		}

		for (auto Import : Imported)
		{
			T3DImportToBlueprintQueue.Remove(Import);
		}

		GetSelectedActors()->DeselectAll();

		if (ImportedActor)
		{
			GetSelectedActors()->Select(ImportedActor);
			Exec(GetEditorWorldContext().World(), TEXT("DELETE"));
		}
	}

	auto Iter = T3DImportQueue.CreateIterator();
	while (Iter)
	{
		const FString& LevelName = Iter.Key();
		const bool LoadAndActivateLevel = (0 < LevelName.Len());
		bool LoadedAndActive = false;

		ULevelStreaming* LevelStreaming = nullptr;

		if (LoadAndActivateLevel)
		{
			if (EditorWorldFromContext && EditorWorldFromContext->WorldComposition)
			{
				const FString PackageNameToLoad = TEXT("/Game/Maps/Dereth/Tile/") + LevelName; // todo: fix hard-coded relative path

				// find, attempt to load and make current the target level
				bool bFound = false;
				LevelStreaming = FLevelUtils::FindStreamingLevel(EditorWorldFromContext, *PackageNameToLoad);
				if (LevelStreaming)
				{
					bFound = true;
					LoadedAndActive = LevelStreaming->IsLevelLoaded() && EditorWorldFromContext->GetCurrentLevel() == LevelStreaming->GetLoadedLevel();
				}

				if (!bFound)
				{
					// world tile model has a crash when adding levels without using the UI
					FSimpleMulticastDelegate NewCurrentLevelBackup = FEditorDelegates::NewCurrentLevel;
					FEditorDelegates::NewCurrentLevel.Clear();

					LevelStreaming = UEditorLevelUtils::CreateNewStreamingLevelForWorld(*EditorWorldFromContext, ULevelStreamingDynamic::StaticClass(), PackageNameToLoad, false, nullptr, false);
					if (LevelStreaming)
					{
						if (EditorWorldFromContext->WorldComposition)
						{
							UWorldComposition::FTilesList& Tiles = EditorWorldFromContext->WorldComposition->GetTilesList();
							for (FWorldCompositionTile& Tile : Tiles)
							{
								if (Tile.Info.Bounds.GetSize().IsNearlyZero())
								{
									UE_LOG_ACUNREALED(Log, TEXT("Tile %s has invalid level bounds"), *Tile.PackageName.ToString());
								}

								// todo: reparent tile?
								/*
								if (Tile.PackageName == *PackageNameToLoad)
								{
									if (Tile.Info.Layer.Name.IsEmpty())
									{
										Tile.Info.Layer.Name = TEXT("Uncategorized");
										Tile.Info.Layer.DistanceStreamingEnabled = true;
									}

									break;
								}
								*/
							}
						}

						bFound = true;
						LevelStreaming->PackageNameToLoad = FName(*PackageNameToLoad);
						LoadedAndActive = LoadAndMakeLevelStreamingActive(LevelStreaming);
						FEditorDelegates::NewCurrentLevel = NewCurrentLevelBackup;
						FEditorDelegates::RefreshAllBrowsers.Broadcast();
						FEditorDelegates::RefreshLevelBrowser.Broadcast();
						//Exec(GetEditorWorldContext().World(), TEXT("WorldBrowser RefreshBrowser"));
						// todo: RefreshBrowser command then Call FEditorDelegates::NewCurrentLevel.Broadcast();

						// add a level bounds
						if (LevelStreaming->GetLoadedLevel() && !LevelStreaming->GetLoadedLevel()->LevelBoundsActor.IsValid())
						{
							FActorSpawnParameters SpawnParameters;
							SpawnParameters.OverrideLevel = LevelStreaming->GetLoadedLevel();
							ALevelBounds* NewLevelBounds = EditorWorldFromContext->SpawnActor<ALevelBounds>(SpawnParameters);
							LevelStreaming->GetLoadedLevel()->LevelBoundsActor = NewLevelBounds;
						}
					}
					else
					{
						FEditorDelegates::NewCurrentLevel = NewCurrentLevelBackup;
					}
				}

				// if we failed, try the next one
				if (!LoadedAndActive)
				{
					++Iter;
					continue;
				}
			}
		}

		// this level has pending payloads
		if (0 < Iter.Value().Num())
		{
			const FACUnrealEdImportPayload ImportPayload = Iter.Value().Pop();

			UE_LOG_ACUNREALED(Log, TEXT("Import %s"), *ImportPayload.FileName);

			FPlatformApplicationMisc::ClipboardCopy(*ImportPayload.Payload);

			FString PastedData;
			FPlatformApplicationMisc::ClipboardPaste(PastedData);

			if (PastedData.Len() > 0)
			{
				Exec(GetEditorWorldContext().World(), TEXT("EDIT PASTE"));

				if (FocusImportedActors && GetSelectedActors())
				{
					if (AActor* SelectedActor = Cast<AActor>(GetSelectedActors()->GetSelectedObject(0)))
					{
						if (2 < SelectedActor->GetComponents().Num())
						{
							FocusQueue.Insert(SelectedActor, 0);
						}
					}
				}
			}
			else // retry
			{
				Iter.Value().Add(ImportPayload);
			}
		}

		// this level has no more payloads, save the level and remove the obligation to do so if not required
		if (0 == Iter.Value().Num())
		{
			if (EditorWorldFromContext && EditorWorldFromContext->WorldComposition)
			{
				EditorWorldFromContext->WorldComposition->Rescan();

				UWorldComposition::FTilesList& Tiles = EditorWorldFromContext->WorldComposition->GetTilesList();
				for (FWorldCompositionTile& Tile : Tiles)
				{
					EditorWorldFromContext->WorldComposition->OnTileInfoUpdated(Tile.PackageName, Tile.Info);
				}
			}

			if (!LoadAndActivateLevel || FEditorFileUtils::SaveCurrentLevel())
			{
				T3DImportQueue.Remove(Iter.Key());
			}
		}

		break;
	}

	if (0 < FocusQueue.Num())
	{
		FocusTime += DeltaSeconds;
		if (1.0f < FocusTime)
		{
			MoveViewportCamerasToActor(*FocusQueue.Pop(), true);
			FocusTime = 0.0f;
		}
	}
}
