using UnrealBuildTool;

public class ACUnrealEditor : ModuleRules
{
	public ACUnrealEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivatePCHHeaderFile = "Public/ACUnrealEditor.h";

		PrivateIncludePathModuleNames.AddRange(
			new string[] 
			{
				"AssetRegistry",
				"AssetTools",
				"BlueprintGraph",
				"MainFrame",
				"WorldBrowser",
			}
		);

		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"ApplicationCore",
				"ACUnrealDatTools",
				"ACUnreal",
				"BlueprintGraph",
				"BlueprintMaterialTextureNodes",
				"Core",
				"CoreUObject",
				"Engine",
				"Landscape",
				"Slate",
				"UnrealEd",
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"EditorScriptingUtilities",
				"InterchangeCore",
				"InterchangeEngine",
				"PropertyEditor",
				"RenderCore",
				"RHI",
				"SlateCore",
				"SourceControl",
				"WorldBrowser",
			}
		);
	}
}
