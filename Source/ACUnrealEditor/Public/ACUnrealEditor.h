#pragma once

#include "UnrealEd.h"
#include "Engine.h"
#include "ParticleDefinitions.h"
#include "SoundDefinitions.h"
#include "Net/UnrealNetwork.h"
#include "ACUnrealClasses.h"
#include "ACUnrealEditorClasses.h"

ACUNREALEDITOR_API DECLARE_LOG_CATEGORY_EXTERN(LogACUnrealEd, All, All);

#define ACUNREALED_LOG_PREFIX TEXT("ACUnrealEd: ")
#define UE_LOG_ACUNREALED(Verbosity, Format, ...) \
{ \
	UE_LOG(LogACUnrealEd, Verbosity, TEXT("%s%s"), ACUNREALED_LOG_PREFIX, *FString::Printf(Format, ##__VA_ARGS__)); \
}
