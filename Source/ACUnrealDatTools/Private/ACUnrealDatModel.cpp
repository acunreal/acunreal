#include "ACUnrealDatTools.h"
#include "ACUnrealByteReader.h"
#include "ACUnrealDatModel.h"
#include "ACUnrealDatFile.h"

bool ValidEnum(EACUnrealPortalModelFlags::Type Value)
{
	const uint32 kAllFlags = EACUnrealPortalModelFlags::kNone | EACUnrealPortalModelFlags::kHasPhysicsBSP | EACUnrealPortalModelFlags::kHasDrawingBSP | EACUnrealPortalModelFlags::kHasDegrade;
	return 0 == (~kAllFlags & Value);
}

bool ValidEnum(EACUnrealPortalModelStippling::Type Value)
{
	switch (Value)
	{
	case EACUnrealPortalModelStippling::kNoStippling:
	case EACUnrealPortalModelStippling::kPositiveStippling:
	//case EACUnrealStippling::kNegativeStippling:
	case EACUnrealPortalModelStippling::kNoPosUVs:
	//case EACUnrealStippling::kNoNegUVs:
		return true;
	}

	return false;
}

bool ValidEnum(EACUnrealPortalModelSides::Type Value)
{
	switch (Value)
	{
	case EACUnrealPortalModelSides::kSingle:
	case EACUnrealPortalModelSides::kDouble:
	case EACUnrealPortalModelSides::kBoth:
		return true;
	}

	return false;
}

bool ValidEnum(EACUnrealPortalModelBSPNode::Type Value)
{
	switch (Value)
	{
	//case EACUnrealBSPNode::kNone: // our fake null
	case EACUnrealPortalModelBSPNode::kLeaf:
	case EACUnrealPortalModelBSPNode::kPortal:
	case EACUnrealPortalModelBSPNode::kBpnn:
	case EACUnrealPortalModelBSPNode::kBPIn:
	case EACUnrealPortalModelBSPNode::kBpIN:
	case EACUnrealPortalModelBSPNode::kBpnN:
	case EACUnrealPortalModelBSPNode::kBPIN:
	case EACUnrealPortalModelBSPNode::kBPnN:
	case EACUnrealPortalModelBSPNode::kUnkn1:
	case EACUnrealPortalModelBSPNode::kUnkn2:
	case EACUnrealPortalModelBSPNode::kUnkn3: // appears in EACUnrealResource::Environment
		return true;
	}

	return false;
}

void ReadBSP(FACUnrealByteReader& Reader, TACUnrealBSPNodePtr& Node, EACUnrealPortalModelBSPTree::Type TreeType)
{
	EACUnrealPortalModelBSPNode::Type NodeType = Reader.ReadValue<EACUnrealPortalModelBSPNode::Type>();
	check(ValidEnum(NodeType));

	if (EACUnrealPortalModelBSPNode::kLeaf == NodeType)
	{
		Node = MakeShared<FACUnrealPortalModelBSPLeaf>(TreeType, Reader);
	}
	else if (EACUnrealPortalModelBSPNode::kPortal == NodeType)
	{
		Node = MakeShared<FACUnrealPortalModelBSPPortal>(TreeType, Reader);
	}
	else
	{
		Node = MakeShared<FACUnrealPortalModelBSPNode>(TreeType, NodeType, Reader);
	}
}

TSet<TACUnrealBSPNodePtr> WalkBSP(TACUnrealBSPNodePtr Node, EACUnrealPortalModelBSPNode::Type NodeType)
{
	TSet<TACUnrealBSPNodePtr> NodesOfType;
	if (Node)
	{
		if (Node->BackChild)
		{
			NodesOfType.Append(WalkBSP(Node->BackChild, NodeType));
		}

		if (Node->FrontChild)
		{
			NodesOfType.Append(WalkBSP(Node->FrontChild, NodeType));
		}

		if (Node->Node == NodeType)
		{
			NodesOfType.Add(Node);
		}
	}

	return NodesOfType;
}

FACUnrealPortalModelVertex::FACUnrealPortalModelVertex()
: Index(INDEX_NONE)
, NumTexCoords(0)
, Position(FVector3f::ZeroVector)
, Normal(FVector3f::ZeroVector)
{
}

FACUnrealPortalModelVertex::FACUnrealPortalModelVertex(const FACUnrealPortalModelVertex& Other)
: Index(Other.Index)
, NumTexCoords(Other.NumTexCoords)
, Position(Other.Position)
, Normal(Other.Normal)
, UVs(Other.UVs)
{
}

FACUnrealPortalModelVertex::FACUnrealPortalModelVertex(FACUnrealByteReader& Reader)
: Index(Reader.ReadUint16())
, NumTexCoords(Reader.ReadUint16())
, Position(Reader.ReadValue<FVector3f>())
, Normal(Reader.ReadValue<FVector3f>())
{
	for (uint16 TexCoordIndex = 0; TexCoordIndex < NumTexCoords; ++TexCoordIndex)
	{
		UVs.Add(Reader.ReadValue<FVector2f>());
	}
}

FACUnrealPortalModelVertex& FACUnrealPortalModelVertex::operator=(const FACUnrealPortalModelVertex& Other)
{
	Index = Other.Index;
	NumTexCoords = Other.NumTexCoords;
	Position = Other.Position;
	Normal = Other.Normal;
	UVs = Other.UVs;
	return *this;
}

FACUnrealPortalModelTriangleFan::FACUnrealPortalModelTriangleFan()
: FanIndex(INDEX_NONE)
, NumIndices(0)
, Stippling(EACUnrealPortalModelStippling::kNoStippling)
, Sides(EACUnrealPortalModelSides::kSingle)
, SurfaceIndex(INDEX_NONE)
, SurfaceUnk(0)
{
}

FACUnrealPortalModelTriangleFan::FACUnrealPortalModelTriangleFan(const FACUnrealPortalModelTriangleFan& Other)
: FanIndex(Other.FanIndex)
, NumIndices(Other.NumIndices)
, Stippling(Other.Stippling)
, Sides(Other.Sides)
, SurfaceIndex(Other.SurfaceIndex)
, SurfaceUnk(Other.SurfaceUnk)
, VertexIndices(Other.VertexIndices)
, TexCoordIndices(Other.TexCoordIndices)
{
}

FACUnrealPortalModelTriangleFan::FACUnrealPortalModelTriangleFan(FACUnrealByteReader& Reader)
: FanIndex(Reader.ReadUint16())
, NumIndices(Reader.ReadUint8())
, Stippling(Reader.ReadValue<EACUnrealPortalModelStippling::Type>())
, Sides(Reader.ReadValue<EACUnrealPortalModelSides::Type>())
, SurfaceIndex(Reader.ReadUint16())
, SurfaceUnk(Reader.ReadUint16())
{
	check(ValidEnum(Stippling));
	check(ValidEnum(Sides));

	VertexIndices.Reserve(NumIndices);
	for (uint8 Index = 0; Index < NumIndices; ++Index)
	{
		VertexIndices.Add(Reader.ReadUint16());
	}

	if (EACUnrealPortalModelStippling::kNoPosUVs != Stippling)
	{
		TexCoordIndices.Reserve(NumIndices);
		for (uint16 Index = 0; Index < NumIndices; ++Index)
		{
			TexCoordIndices.Add(Reader.ReadUint8());
		}
	}

	if (EACUnrealPortalModelSides::kBoth == Sides)
	{
		for (uint8 Index = 0; Index < NumIndices; ++Index)
		{
			Reader.ReadUint8();
		}
	}
}

FACUnrealPortalModelTriangleFan& FACUnrealPortalModelTriangleFan::operator=(const FACUnrealPortalModelTriangleFan& Other)
{
	FanIndex = Other.FanIndex;
	NumIndices = Other.NumIndices;
	Stippling = Other.Stippling;
	Sides = Other.Sides;
	SurfaceIndex = Other.SurfaceIndex;
	SurfaceUnk = Other.SurfaceUnk;
	VertexIndices = Other.VertexIndices;
	TexCoordIndices = Other.TexCoordIndices;
	return *this;
}

bool FACUnrealPortalModelTriangleFan::operator<(const FACUnrealPortalModelTriangleFan& Other) const
{
	return (SurfaceIndex < Other.SurfaceIndex) || ((SurfaceIndex == Other.SurfaceIndex) && (FanIndex < Other.FanIndex));
}

FACUnrealPortalModelBSPNode::FACUnrealPortalModelBSPNode(EACUnrealPortalModelBSPTree::Type TreeType, EACUnrealPortalModelBSPNode::Type PlaneType)
: Tree(TreeType)
, Node(PlaneType)
{
}

FACUnrealPortalModelBSPNode::FACUnrealPortalModelBSPNode(const FACUnrealPortalModelBSPNode& Other)
: Tree(Other.Tree)
, Node(Other.Node)
, Bounds(Other.Bounds)
, Partition(Other.Partition)
, FrontChild(Other.FrontChild)
, BackChild(Other.BackChild)
, TriangleIndices(Other.TriangleIndices)
{
}

FACUnrealPortalModelBSPNode::FACUnrealPortalModelBSPNode(EACUnrealPortalModelBSPTree::Type TreeType, EACUnrealPortalModelBSPNode::Type PlaneType, FACUnrealByteReader& Reader)
: Tree(TreeType)
, Node(PlaneType)
{
	const FVector3f PartitionNormal = Reader.ReadValue<FVector3f>();
	const float ParitionW = Reader.ReadFloat();
	Partition = FPlane4f(PartitionNormal, ParitionW);

	if (EACUnrealPortalModelBSPNode::kBpnn == Node || EACUnrealPortalModelBSPNode::kBPIn == Node)
	{
		ReadBSP(Reader, FrontChild, Tree);
	}
	else if (EACUnrealPortalModelBSPNode::kBpIN == Node || EACUnrealPortalModelBSPNode::kBpnN == Node)
	{
		ReadBSP(Reader, BackChild, Tree);
	}
	else if (EACUnrealPortalModelBSPNode::kBPIN == Node || EACUnrealPortalModelBSPNode::kBPnN == Node)
	{
		ReadBSP(Reader, FrontChild, Tree);
		ReadBSP(Reader, BackChild, Tree);
	}

	if (EACUnrealPortalModelBSPTree::kDrawing == Tree || EACUnrealPortalModelBSPTree::kPhysics == Tree)
	{
		const FVector3f BoundsCenter = Reader.ReadValue<FVector3f>();
		const float BoundsRadius = Reader.ReadFloat();
		Bounds = FSphere3f(BoundsCenter, BoundsRadius);
	}

	if (EACUnrealPortalModelBSPTree::kDrawing == Tree)
	{
		uint32 triCount = Reader.ReadUint32();
		TriangleIndices.Reserve(triCount);

		for (uint32 TriangleIndex = 0; TriangleIndex < triCount; ++TriangleIndex)
		{
			TriangleIndices.Add(Reader.ReadUint16());
		}
	}
}

FACUnrealPortalModelBSPNode& FACUnrealPortalModelBSPNode::operator=(const FACUnrealPortalModelBSPNode& Other)
{
	Tree = Other.Tree;
	Node = Other.Node;
	Bounds = Other.Bounds;
	Partition = Other.Partition;
	FrontChild = Other.FrontChild;
	BackChild = Other.BackChild;
	TriangleIndices = Other.TriangleIndices;
	return *this;
}

FACUnrealPortalModelBSPLeaf::FACUnrealPortalModelBSPLeaf(EACUnrealPortalModelBSPTree::Type TreeType)
: FACUnrealPortalModelBSPNode(TreeType, EACUnrealPortalModelBSPNode::kLeaf)
, Index(INDEX_NONE)
, Solid(INDEX_NONE)
{
}

FACUnrealPortalModelBSPLeaf::FACUnrealPortalModelBSPLeaf(const FACUnrealPortalModelBSPLeaf& Other)
: FACUnrealPortalModelBSPNode(Other)
, Index(Other.Index)
, Solid(Other.Solid)
{
}

FACUnrealPortalModelBSPLeaf::FACUnrealPortalModelBSPLeaf(EACUnrealPortalModelBSPTree::Type TreeType, FACUnrealByteReader& Reader)
: FACUnrealPortalModelBSPNode(TreeType, EACUnrealPortalModelBSPNode::kLeaf)
, Index(Reader.ReadUint32())
, Solid(0)
{
	if(EACUnrealPortalModelBSPTree::kPhysics == Tree)
	{
		Solid = Reader.ReadUint32();

		const FVector3f BoundsCenter = Reader.ReadValue<FVector3f>();
		const float BoundsRadius = Reader.ReadFloat();
		Bounds = FSphere3f(BoundsCenter, BoundsRadius);

		uint32 triCount = Reader.ReadUint32();
		TriangleIndices.Reserve(triCount);

		for (uint32 TriangleIndex = 0; TriangleIndex < triCount; ++TriangleIndex)
		{
			TriangleIndices.Add(Reader.ReadUint16());
		}
	}
}

FACUnrealPortalModelBSPLeaf& FACUnrealPortalModelBSPLeaf::operator=(const FACUnrealPortalModelBSPLeaf& Other)
{
	FACUnrealPortalModelBSPNode::operator=(Other);
	Index = Other.Index;
	Solid = Other.Solid;
	return *this;
}

FACUnrealPortalModelBSPPortalPoly::FACUnrealPortalModelBSPPortalPoly(uint16 InPortalIndex, uint16 InPolygonIndex)
: PortalIndex(InPortalIndex)
, PolygonIndex(InPolygonIndex)
{
}

FACUnrealPortalModelBSPPortalPoly::FACUnrealPortalModelBSPPortalPoly(const FACUnrealPortalModelBSPPortalPoly& Other)
: PortalIndex(Other.PortalIndex)
, PolygonIndex(Other.PolygonIndex)
{
}

FACUnrealPortalModelBSPPortalPoly::FACUnrealPortalModelBSPPortalPoly(FACUnrealByteReader& Reader)
: PortalIndex(Reader.ReadUint16())
, PolygonIndex(Reader.ReadUint16())
{
}

FACUnrealPortalModelBSPPortalPoly& FACUnrealPortalModelBSPPortalPoly::operator=(const FACUnrealPortalModelBSPPortalPoly& Other)
{
	PortalIndex = Other.PortalIndex;
	PolygonIndex = Other.PolygonIndex;
	return *this;
}

FACUnrealPortalModelBSPPortal::FACUnrealPortalModelBSPPortal(EACUnrealPortalModelBSPTree::Type TreeType)
: FACUnrealPortalModelBSPNode(TreeType, EACUnrealPortalModelBSPNode::kPortal)
{
}

FACUnrealPortalModelBSPPortal::FACUnrealPortalModelBSPPortal(const FACUnrealPortalModelBSPPortal& Other)
: FACUnrealPortalModelBSPNode(Other)
, PortalPolys(Other.PortalPolys)
{
}

FACUnrealPortalModelBSPPortal::FACUnrealPortalModelBSPPortal(EACUnrealPortalModelBSPTree::Type TreeType, FACUnrealByteReader& Reader)
: FACUnrealPortalModelBSPNode(TreeType, EACUnrealPortalModelBSPNode::kPortal)
{
	const FVector3f PartitionNormal = Reader.ReadValue<FVector3f>();
	const float ParitionW = Reader.ReadFloat();
	Partition = FPlane4f(PartitionNormal, ParitionW);
	ReadBSP(Reader, FrontChild, Tree);
	ReadBSP(Reader, BackChild, Tree);

	if (EACUnrealPortalModelBSPTree::kDrawing == Tree)
	{
		const FVector3f BoundsCenter = Reader.ReadValue<FVector3f>();
		const float BoundsRadius = Reader.ReadFloat();
		Bounds = FSphere3f(BoundsCenter, BoundsRadius);

		uint32 TriCount = Reader.ReadUint32();
		TriangleIndices.Reserve(TriCount);

		uint32 polyCount = Reader.ReadUint32();
		PortalPolys.Reserve(polyCount);

		for (uint32 TriangleIndex = 0; TriangleIndex < TriCount; ++TriangleIndex)
		{
			TriangleIndices.Add(Reader.ReadUint16());
		}

		for (uint32 PolyIndex = 0; PolyIndex < polyCount; ++PolyIndex)
		{
			uint16 PortalIndex = Reader.ReadUint16();
			uint16 PolygonIndex = Reader.ReadUint16();
			PortalPolys.Add(FACUnrealPortalModelBSPPortalPoly(PortalIndex, PolygonIndex));
		}
	}
}

FACUnrealPortalModelBSPPortal& FACUnrealPortalModelBSPPortal::operator=(const FACUnrealPortalModelBSPPortal& Other)
{
	FACUnrealPortalModelBSPNode::operator=(Other);
	PortalPolys = Other.PortalPolys;
	return *this;
}

FACUnrealPortalModel::FACUnrealPortalModel()
: ResourceId(INDEX_NONE)
, Flags(EACUnrealPortalModelFlags::kNone)
, SurfaceCount(0)
, NumCollisionFans(0)
, NumRenderFans(0)
{
}

FACUnrealPortalModel::FACUnrealPortalModel(const FACUnrealPortalModel& Other)
: ResourceId(Other.ResourceId)
, Flags(Other.Flags)
, SurfaceCount(Other.SurfaceCount)
, SurfaceIDs(Other.SurfaceIDs)
, Vertices(Other.Vertices)
, NumCollisionFans(Other.NumCollisionFans)
, CollisionFans(Other.CollisionFans)
, CollisionBSP(Other.CollisionBSP)
, NumRenderFans(Other.NumRenderFans)
, RenderFans(Other.RenderFans)
, RenderBSP(Other.RenderBSP)
, PortalNodes(Other.PortalNodes)
{
}

FACUnrealPortalModel::FACUnrealPortalModel(FACUnrealByteReader& Reader)
: ResourceId(Reader.ReadUint32())
, Flags(Reader.ReadValue<EACUnrealPortalModelFlags::Type>())
, SurfaceCount(Reader.ReadUint8())
, NumCollisionFans(0)
, NumRenderFans(0)
{
	check(ValidEnum(Flags));

	for (uint8 SurfaceIndex = 0; SurfaceIndex < SurfaceCount; ++SurfaceIndex)
	{
		SurfaceIDs.Add(Reader.ReadUint32());
	}

	uint32 AlwaysOne = Reader.ReadUint32();
	check(1 == AlwaysOne);

	uint16 NumVertices = Reader.ReadUint16();
	uint16 flags2 = Reader.ReadUint16();
	check(0 == flags2 || 0x8000 == flags2);

	for (uint16 VertexIndex = 0; VertexIndex < NumVertices; ++VertexIndex)
	{
		Vertices.Add(FACUnrealPortalModelVertex(Reader));
	}

	if (EACUnrealPortalModelFlags::kHasPhysicsBSP & Flags)
	{
		NumCollisionFans = Reader.ReadPackedUint16();
		CollisionFans.Reserve(NumCollisionFans);
		for (uint16 CollisionFan = 0; CollisionFan < NumCollisionFans; ++CollisionFan)
		{
			CollisionFans.Add(FACUnrealPortalModelTriangleFan(Reader));
		}

		ReadBSP(Reader, CollisionBSP, EACUnrealPortalModelBSPTree::kPhysics);
	}

	FVector3f SortCenter = Reader.ReadValue<FVector3f>();

	if (EACUnrealPortalModelFlags::kHasDrawingBSP & Flags)
	{
		NumRenderFans = Reader.ReadPackedUint16();
		RenderFans.Reserve(NumRenderFans);
		for (uint16 RenderFan = 0; RenderFan < NumRenderFans; ++RenderFan)
		{
			RenderFans.Add(FACUnrealPortalModelTriangleFan(Reader));
		}

		ReadBSP(Reader, RenderBSP, EACUnrealPortalModelBSPTree::kDrawing);

		TSet<TACUnrealBSPNodePtr> RenderPortalNodes = WalkBSP(RenderBSP, EACUnrealPortalModelBSPNode::kPortal);
		if (0 < RenderPortalNodes.Num())
		{
			PortalNodes.Append(RenderPortalNodes.Array());
		}
	}

	if (EACUnrealPortalModelFlags::kHasDegrade & Flags)
	{
		uint32 HasDegrade = Reader.ReadUint32();
	}

	check(0 == Reader.BytesLeft());
}

FACUnrealPortalModel& FACUnrealPortalModel::operator=(const FACUnrealPortalModel& Other)
{
	ResourceId = Other.ResourceId;
	Flags = Other.Flags;
	SurfaceCount = Other.SurfaceCount;
	SurfaceIDs = Other.SurfaceIDs;
	Vertices = Other.Vertices;
	NumCollisionFans = Other.NumCollisionFans;
	CollisionFans = Other.CollisionFans;
	CollisionBSP = Other.CollisionBSP;
	NumRenderFans = Other.NumRenderFans;
	RenderFans = Other.RenderFans;
	RenderBSP = Other.RenderBSP;
	PortalNodes = Other.PortalNodes;
	return *this;
}
