#include "ACUnrealDatTools.h"
#include "ACUnrealByteReader.h"
#include "ACUnrealDatPalette.h"
#include "ACUnrealDatFile.h"

FACUnrealPortalPalette::FACUnrealPortalPalette()
: ResourceId(0)
, NumColors(0)
{
}

FACUnrealPortalPalette::FACUnrealPortalPalette(FACUnrealByteReader& Reader)
: ResourceId(Reader.ReadUint32())
, NumColors(Reader.ReadUint32())
{
	check(NumColors == 2048);
	Colors.AddZeroed(NumColors);

	for (FColor& Color : Colors)
	{
		Color.B = Reader.ReadUint8();
		Color.G = Reader.ReadUint8();
		Color.R = Reader.ReadUint8();
		Color.A = Reader.ReadUint8();
	}

	check(Reader.BytesLeft() == 0);
}
