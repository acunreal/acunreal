#include "ACUnrealDatTools.h"
#include "ACUnrealByteReader.h"
#include "ACUnrealDatEnvironment.h"
#include "ACUnrealDatFile.h"

FACUnrealPortalEnvironmentPart::FACUnrealPortalEnvironmentPart()
{
}

FACUnrealPortalEnvironmentPart::FACUnrealPortalEnvironmentPart(FACUnrealByteReader& Reader)
{
	uint32 NumTriangleFans = Reader.ReadUint32();
	TriangleFans.Reserve(NumTriangleFans);

	uint32 NumHitTriangleFans = Reader.ReadUint32();
	HitTriangleFans.Reserve(NumHitTriangleFans);

	uint32 NumPortals = Reader.ReadUint32();

	uint32 Unk1 = Reader.ReadUint32();
	check(Unk1 == 1);

	uint32 NumVertices = Reader.ReadUint32();
	Vertices.Reserve(NumVertices);

	for (uint32 i = 0; i < NumVertices; i++)
	{
		Vertices.Add(FACUnrealPortalModelVertex(Reader));
	}

	for (uint32 i = 0; i < NumTriangleFans; i++)
	{
		TriangleFans.Add(FACUnrealPortalModelTriangleFan(Reader));
	}

	for (uint32 i = 0; i < NumPortals; i++)
	{
		Portals.Add(Reader.ReadUint16());
	}

	Reader.Align();

	TACUnrealBSPNodePtr CellBSP;
	ReadBSP(Reader, CellBSP,  EACUnrealPortalModelBSPTree::kCell);

	for (uint32 i = 0; i < NumHitTriangleFans; i++)
	{
		HitTriangleFans.Add(FACUnrealPortalModelTriangleFan(Reader));
	}

	TACUnrealBSPNodePtr physicsBSP;
	ReadBSP(Reader, CellBSP, EACUnrealPortalModelBSPTree::kPhysics);

	uint32 HasDrawingBSP = Reader.ReadUint32();
	check(HasDrawingBSP == 0 || HasDrawingBSP == 1);

	if (HasDrawingBSP)
	{
		TACUnrealBSPNodePtr DrawingBSP;
		ReadBSP(Reader, DrawingBSP, EACUnrealPortalModelBSPTree::kDrawing);
	}

	Reader.Align();
}

TArray<uint16> FACUnrealPortalEnvironmentPart::GetSurfaceIndices(EACUnrealPortalEnvironmentPartSurface::Type SurfaceType) const
{
	TSet<uint16> SurfaceIndices;
	const TArray<FACUnrealPortalModelTriangleFan>& Fans = (EACUnrealPortalEnvironmentPartSurface::kHitTriangles == SurfaceType) ? HitTriangleFans : TriangleFans;
	int32 LastSurfaceIndex = INDEX_NONE;

	if (EACUnrealPortalEnvironmentPartSurface::kPortals == SurfaceType)
	{
		const TArray<uint16>& PortalFans = Portals;
		for (int32 TriangleFanIndex = 0; TriangleFanIndex < PortalFans.Num(); ++TriangleFanIndex)
		{
			const uint16 SurfaceIndex = Fans[PortalFans[TriangleFanIndex]].SurfaceIndex;
			if (SurfaceIndex != LastSurfaceIndex || INDEX_NONE == LastSurfaceIndex)
			{
				LastSurfaceIndex = SurfaceIndex;
				SurfaceIndices.Add(SurfaceIndex);
			}
		}
	}
	else
	{
		for (int32 TriangleFanIndex = 0; TriangleFanIndex < Fans.Num(); ++TriangleFanIndex)
		{
			const uint16 SurfaceIndex = Fans[TriangleFanIndex].SurfaceIndex;
			if (SurfaceIndex != LastSurfaceIndex || INDEX_NONE == LastSurfaceIndex)
			{
				LastSurfaceIndex = SurfaceIndex;
				SurfaceIndices.Add(SurfaceIndex);
			}
		}
	}

	TArray<uint16> AsArray = SurfaceIndices.Array();
	AsArray.Sort();
	return AsArray;
}

FACUnrealPortalEnvironment::FACUnrealPortalEnvironment()
{
}

FACUnrealPortalEnvironment::FACUnrealPortalEnvironment(FACUnrealByteReader& Reader)
: ResourceId(Reader.ReadUint32())
, NumParts(Reader.ReadUint32())
{
	Parts.Reserve(NumParts);

	for (uint32 PartIndex = 0; PartIndex < NumParts; ++PartIndex)
	{
		const uint32 PartIndexRedundant = Reader.ReadUint32();
		Parts.Add(FACUnrealPortalEnvironmentPart(Reader));
	}

	check(0 == Reader.BytesLeft());
}
