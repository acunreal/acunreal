#include "ACUnrealDatTools.h"
#include "ACUnrealT3dWriter.h"
#include "ACUnrealCellFile.h"
#include "ACUnrealPortalFile.h"
#include "ACUnrealDatEnvironment.h"
#include "ACUnrealDatModel.h"
#include "ACUnrealDatScene.h"
#include "ACUnrealDatSetup.h"
#include "ACUnrealDatRegion.h"
#include "LandscapeDataAccess.h"
#include "Misc/DateTime.h"
#include "Modules/ModuleManager.h"

const float kStructureExEnvironmentZBump = 0.001f; // 1mm

static FVector3f ToUnrealLocation(const FACUnrealResourceLocation& Location) { return FVector3f(Location.OffsetX, -Location.OffsetY, Location.OffsetZ) * 100.0f; }

FACUnrealT3dContent::FACUnrealT3dContent(int32 InDepth)
: FACUnrealT3dContent(TEXT("Invalid: Invalid\r\n"), InDepth)
{
}

FACUnrealT3dContent::FACUnrealT3dContent(const FString& InContent, int32 InTabDepth)
: Content(InContent)
, TabDepth(InTabDepth)
, PrevDepth(FMath::Max(0, InTabDepth - 1))
, NextDepth(InTabDepth + 1)
{
	if (0 < TabDepth)
	{
		for (int32 i = 0; i < InTabDepth; ++i)
		{
			Indent += TEXT("\t");
		}

		Content.InsertAt(0, Indent);
	}
}

FACUnrealT3dSection::FACUnrealT3dSection(const FString& InSectionTag, const FString& InSectionValue, int32 InTabDepth)
: FACUnrealT3dContent(FString::Printf(TEXT("Begin %s%s\r\n"), *InSectionTag, *InSectionValue), InTabDepth)
, SectionTag(InSectionTag)
{
}

FACUnrealT3dSection::operator FString() const
{
	FString OutContent = Content;
	AppendBody(OutContent);
	AppendFooter(OutContent);
	return OutContent;
}

void FACUnrealT3dSection::AppendFooter(FString& OutContent) const
{
	OutContent.Append(FString(FString::Printf(TEXT("%sEnd %s\r\n"), *Indent, *SectionTag)));
}

struct FACUnrealT3dSceneComponent : public FACUnrealT3dSection
{
public:
	FACUnrealT3dSceneComponent(const FACUnrealResourceLocation& InLocation, const FString& InIdentifier, const FString& InAttachParentType, const FString& InAttachParent, bool InUseAttachParentBounds, int32 InTabDepth)
	: FACUnrealT3dSection(TEXT("Object"), FString::Printf(TEXT(" Name=\"%s\""), *InIdentifier), InTabDepth)
	, Location(InLocation)
	, AttachParentType(InAttachParentType)
	, AttachParent(InAttachParent)
	, bUseAttachParentBounds(InUseAttachParentBounds)
	{
	}

	FACUnrealT3dSceneComponent(const FACUnrealResourceLocation& InLocation, int32 InIdentifier, int32 InIndex, int32 InDepth, const FString& InAttachParentType, const FString& InAttachParent, bool InUseAttachParentBounds, const FString& InIdentifierPrefix = TEXT(""))
	: FACUnrealT3dSection(TEXT("Object"), FString::Printf(TEXT(" Name=\"%s%d_0x%08X\""), *InIdentifierPrefix, InIndex, InIdentifier), InDepth)
	, Location(InLocation)
	, AttachParentType(InAttachParentType)
	, AttachParent(InAttachParent)
	, bUseAttachParentBounds(InUseAttachParentBounds)
	{
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		const FVector3f UnrealLocation = ToUnrealLocation(Location);
		FQuat4f UnrealQuat = FQuat4f(-Location.HeadingA, Location.HeadingB, -Location.HeadingC, Location.HeadingW);
		UnrealQuat.Normalize();

		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("AttachParent=%s'\"%s\"'\r\n"), *AttachParentType, *AttachParent), NextDepth)));

		if (bUseAttachParentBounds)
		{
			OutContent.Append(FString(FACUnrealT3dContent(TEXT("bUseAttachParentBound=True\r\n"), NextDepth)));
		}

		if (!UnrealLocation.IsNearlyZero())
		{
			OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("RelativeLocation=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), UnrealLocation.X, UnrealLocation.Y, UnrealLocation.Z), NextDepth)));
		}

		if (!UnrealQuat.Rotator().IsNearlyZero())
		{
			OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("RelativeRotation=(Pitch=%1.6f,Yaw=%1.6f,Roll=%1.6f)\r\n"), UnrealQuat.Rotator().Pitch, UnrealQuat.Rotator().Yaw, UnrealQuat.Rotator().Roll), NextDepth)));
		}

		// TODO: maybe scale here?
		//OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("RelativeScale3D=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), Location.Scale, Location.Scale, Location.Scale), NextDepth)));

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CreationMethod=Instance\r\n"), NextDepth)));
	}

	FACUnrealResourceLocation Location;
	FString AttachParentType;
	FString AttachParent;
	bool bUseAttachParentBounds;
	float Scale = 1.0f;
};

struct FACUnrealT3dLightComponent : public FACUnrealT3dSceneComponent
{
public:
	FACUnrealT3dLightComponent(const FACUnrealSetupLight& InLight, const FACUnrealResourceLocation& InLocation, int32 InIndex, int32 InTabDepth, const FString& InAttachParentType, const FString& InAttachParent)
	: FACUnrealT3dSceneComponent(InLocation, FString::Printf(TEXT("Light_%03d"), InIndex), InAttachParentType, InAttachParent, false, InTabDepth)
	, Light(InLight)
	, AttachParent(InAttachParent)
	{
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("AttenuationRadius=%1.6f\r\n"), Light.Falloff * 100.0f), NextDepth)));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("Intensity=%1.6f\r\n"), Light.Intensity), NextDepth)));
		const uint8* const pColor = reinterpret_cast<const uint8*>(&Light.Color);
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("LightColor=(B=%d,G=%d,R=%d,A=%d)\r\n"), pColor[0], pColor[1], pColor[2], pColor[3]), NextDepth)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CastShadows=False\r\n"), NextDepth)));
		FACUnrealT3dSceneComponent::AppendBody(OutContent);
	}

	FACUnrealSetupLight Light;
	FString AttachParent;
};

struct FACUnrealT3dLandblockStaticObject : public FACUnrealT3dSceneComponent
{
public:
	FACUnrealT3dLandblockStaticObject(const FACUnrealResourceInfo& InStaticObject, int32 InIndex, int32 InTabDepth, const FString& InAttachParentType, const FString& InAttachParent, bool InUseAttachParentBounds, bool InOccluder, const FString& InIdentifierPrefix = TEXT(""), const TArray<uint32>& InOverrideMaterials = {})
	: FACUnrealT3dSceneComponent(InStaticObject.Location, FString::Printf(TEXT("%s%d_0x%08X"), *InIdentifierPrefix, InIndex, InStaticObject.ResourceId), InAttachParentType, InAttachParent, InUseAttachParentBounds, InTabDepth)
	, StaticObject(InStaticObject)
	, AttachParent(InAttachParent)
	, OverrideMaterials(InOverrideMaterials)
	, bOccluder(InOccluder)
	{
	}

	FACUnrealT3dLandblockStaticObject(const FACUnrealResourceInfo& InStaticObject, int32 InIndex, int32 InSubIndex, int32 InTabDepth, const FString& InAttachParentType, const FString& InAttachParent, bool InUseAttachParentBounds, bool InOccluder, const FString& InIdentifierPrefix = TEXT(""), const TArray<uint32>& InOverrideMaterials = {})
	: FACUnrealT3dSceneComponent(InStaticObject.Location, FString::Printf(TEXT("%s%d_0x%08X_%d"), *InIdentifierPrefix, InIndex, InStaticObject.ResourceId, InSubIndex), InAttachParentType, InAttachParent, InUseAttachParentBounds, InTabDepth)
	, StaticObject(InStaticObject)
	, AttachParent(InAttachParent)
	, OverrideMaterials(InOverrideMaterials)
	, bOccluder(InOccluder)
	{
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		FString ResourcePath;
		if (EACUnrealResource_Native::kEnvironment == (StaticObject.ResourceId & EACUnrealResource_Native::kMask))
		{
			ResourcePath = TEXT("Environments");
		}
		else
		{
			ResourcePath = TEXT("Models");
		}

		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("StaticMesh=StaticMesh'\"/Game/%s/0x%08X.0x%08X\"'\r\n"), *ResourcePath, StaticObject.ResourceId, StaticObject.ResourceId), NextDepth)));

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("StaticMeshImportVersion=1\r\n"), NextDepth)));

		for (int32 OverrideMaterialIndex = 0; OverrideMaterialIndex < OverrideMaterials.Num(); ++OverrideMaterialIndex)
		{
			const uint32 SurfaceId = EACUnrealResource_Native::kSurface | OverrideMaterials[OverrideMaterialIndex];
			OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("OverrideMaterials(%d)=MaterialInstanceConstant'\"/Game/Surfaces/0x%08X.0x%08X\"'\r\n"), OverrideMaterialIndex, SurfaceId, SurfaceId), NextDepth)));
		}

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bGenerateOverlapEvents=False\r\n"), NextDepth)));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("bUseAsOccluder=%s\r\n"), bOccluder ? TEXT("True") : TEXT("False")), NextDepth)));

		FACUnrealT3dSceneComponent::AppendBody(OutContent);
	}

	FACUnrealResourceInfo StaticObject;
	FString AttachParent;
	TArray<uint32> OverrideMaterials;
	bool bOccluder;
};

struct FACUnrealT3dLandblockStaticObjectInstanced : public FACUnrealT3dSceneComponent
{
public:
	FACUnrealT3dLandblockStaticObjectInstanced(uint32 InResourceId, int32 InIndex, int32 InTabDepth, const FString& InAttachParentType, const FString& InAttachParent, bool InUseAttachParentBounds, bool InOccluder, const FString& InIdentifierPrefix = TEXT(""), const TArray<uint32>& InOverrideMaterials = {})
	: FACUnrealT3dSceneComponent(FACUnrealResourceLocation(), FString::Printf(TEXT("%s%d_0x%08X_Instanced"), *InIdentifierPrefix, InIndex, InResourceId), InAttachParentType, InAttachParent, InUseAttachParentBounds, InTabDepth)
	, ResourceId(InResourceId)
	, Index(InIndex)
	, OverrideMaterials(InOverrideMaterials)
	, bOccluder(InOccluder)
	{
	}

	void AddInstance(const FACUnrealResourceLocation& InstanceLocation)
	{
		Instances.Add(InstanceLocation);
	}

	void AppendPerInstanceData(FString& OutContent) const
	{
		for (int32 InstanceIndex = 0; InstanceIndex < Instances.Num(); ++InstanceIndex)
		{
			const FACUnrealResourceLocation& InstanceLocation = Instances[InstanceIndex];
			const FVector3f UnrealLocation = ToUnrealLocation(InstanceLocation);
			FQuat4f UnrealQuat = FQuat4f(-InstanceLocation.HeadingA, InstanceLocation.HeadingB, -InstanceLocation.HeadingC, InstanceLocation.HeadingW);
			UnrealQuat.Normalize();

			// TODO: maybe scale here?
			// todo: these planes should come from a transform so they can consider scale
			const FString XPlane = FString::Printf(TEXT("(W=%1.6f,X=%1.6f,Y=%1.6f,Z=%1.6f)"), 0.0f, UnrealQuat.GetAxisX().X, UnrealQuat.GetAxisX().Y, UnrealQuat.GetAxisX().Z);
			const FString YPlane = FString::Printf(TEXT("(W=%1.6f,X=%1.6f,Y=%1.6f,Z=%1.6f)"), 0.0f, UnrealQuat.GetAxisY().X, UnrealQuat.GetAxisY().Y, UnrealQuat.GetAxisY().Z);
			const FString ZPlane = FString::Printf(TEXT("(W=%1.6f,X=%1.6f,Y=%1.6f,Z=%1.6f)"), 0.0f, UnrealQuat.GetAxisZ().X, UnrealQuat.GetAxisZ().Y, UnrealQuat.GetAxisZ().Z);
			const FString WPlane = FString::Printf(TEXT("(W=%1.6f,X=%1.6f,Y=%1.6f,Z=%1.6f)"), 1.0f, UnrealLocation.X, UnrealLocation.Y, UnrealLocation.Z);
			OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("PerInstanceSMData(%d)=(Transform=(XPlane=%s,YPlane=%s,ZPlane=%s,WPlane=%s))\r\n"), InstanceIndex, *XPlane, *YPlane, *ZPlane, *WPlane), NextDepth)));
		}
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		FString ResourcePath;
		if (EACUnrealResource_Native::kEnvironment == (ResourceId & EACUnrealResource_Native::kMask))
		{
			ResourcePath = TEXT("Environments");
		}
		else
		{
			ResourcePath = TEXT("Models");
		}

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("InstancingRandomSeed=0\r\n"), NextDepth)));

		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("StaticMesh=StaticMesh'\"/Game/%s/0x%08X.0x%08X\"'\r\n"), *ResourcePath, ResourceId, ResourceId), NextDepth)));

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("StaticMeshImportVersion=1\r\n"), NextDepth)));

		for (int32 OverrideMaterialIndex = 0; OverrideMaterialIndex < OverrideMaterials.Num(); ++OverrideMaterialIndex)
		{
			const uint32 SurfaceId = EACUnrealResource_Native::kSurface | OverrideMaterials[OverrideMaterialIndex];
			OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("OverrideMaterials(%d)=MaterialInstanceConstant'\"/Game/Surfaces/0x%08X.0x%08X\"'\r\n"), OverrideMaterialIndex, SurfaceId, SurfaceId), NextDepth)));
		}

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bGenerateOverlapEvents=False\r\n"), NextDepth)));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("bUseAsOccluder=%s\r\n"), bOccluder ? TEXT("True") : TEXT("False")), NextDepth)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bHasPerInstanceHitProxies=True\r\n"), NextDepth)));

		FACUnrealT3dSceneComponent::AppendBody(OutContent);
	}

	uint32 ResourceId;
	int32 Index;
	TArray<uint32> OverrideMaterials;
	TArray<FACUnrealResourceLocation> Instances;
	bool bOccluder;
};

void AppendResourceHeader(const FACUnrealPortalFile& PortalFile, const FACUnrealResourceInfo& ResourceInfo, const FString& NamePrefix, int32 ObjectIndex, int32 NextDepth, FString& OutContent)
{
	switch (ResourceInfo.ResourceId & EACUnrealResource_Native::kMask)
	{
		case EACUnrealResource_Native::kEnvironment:
		case EACUnrealResource_Native::kModel:
			OutContent.Append(FString(FACUnrealT3dSection(TEXT("Object"), FString::Printf(TEXT(" Class=/Script/Engine.StaticMeshComponent Name=\"%s%d_0x%08X\""), *NamePrefix, ObjectIndex, ResourceInfo.ResourceId), NextDepth + 2)));
			break;

		case EACUnrealResource_Native::kSetup:
		{
			FACUnrealPortalSetup SetupObject;
			if (PortalFile.GetResource(ResourceInfo.ResourceId, SetupObject))
			{
				for (int32 ModelResourceIndex = 0; ModelResourceIndex < SetupObject.ModelResourceIds.Num(); ++ModelResourceIndex)
				{
					const int32 ModelResourceId = SetupObject.ModelResourceIds[ModelResourceIndex];
					OutContent.Append(FString(FACUnrealT3dSection(TEXT("Object"), FString::Printf(TEXT(" Class=/Script/Engine.StaticMeshComponent Name=\"%s%d_0x%08X_%d\""), *NamePrefix, ObjectIndex, ModelResourceId, ModelResourceIndex), NextDepth + 2)));
				}

				for (int32 LightIndex = 0; LightIndex < SetupObject.Lights.Num(); ++LightIndex)
				{
					OutContent.Append(FString(FACUnrealT3dSection(TEXT("Object"), FString::Printf(TEXT(" Class=/Script/Engine.PointLightComponent Name=\"Light_%03d\""), ObjectIndex + LightIndex), NextDepth + 2)));
				}
			}
		}
		break;

		case EACUnrealResource_Native::kScene:
		{
			FACUnrealPortalScene Scene;
			if (PortalFile.GetResource(ResourceInfo.ResourceId, Scene))
			{
				for (int32 SceneObjectIndex = 0; SceneObjectIndex < Scene.Objects.Num(); ++SceneObjectIndex)
				{
					const FACUnrealPortalSceneObject& SceneObject = Scene.Objects[SceneObjectIndex];
					AppendResourceHeader(PortalFile, SceneObject.ResourceInfo, NamePrefix, ObjectIndex, NextDepth, OutContent);
				}
			}
		}
		break;
	}
}

void AppendResourceBody(const FACUnrealPortalFile& PortalFile, const FACUnrealResourceInfo& ResourceInfo, const FString& NamePrefix, int32 ObjectIndex, int32 NextDepth, FString& OutContent, const TArray<uint32>& EnvironmentSurfaceIds = {})
{
	const bool bInParentsBounds = (FMath::Abs(ResourceInfo.Location.OffsetX) < 96.0f) && (FMath::Abs(ResourceInfo.Location.OffsetY) < 96.0f);

	switch (ResourceInfo.ResourceId & EACUnrealResource_Native::kMask)
	{
		case EACUnrealResource_Native::kEnvironment:
		{
			OutContent.Append(FString(FACUnrealT3dLandblockStaticObject(ResourceInfo, ObjectIndex, NextDepth + 2, TEXT("SceneComponent"), TEXT("DefaultSceneRoot"), bInParentsBounds, true, NamePrefix, EnvironmentSurfaceIds)));
		}
		break;

		case EACUnrealResource_Native::kModel:
		{
			OutContent.Append(FString(FACUnrealT3dLandblockStaticObject(ResourceInfo, ObjectIndex, NextDepth + 2, TEXT("SceneComponent"), TEXT("DefaultSceneRoot"), bInParentsBounds, false, NamePrefix)));
		}
		break;

		case EACUnrealResource_Native::kSetup:
		{
			FACUnrealPortalSetup SetupObject;
			if (PortalFile.GetResource(ResourceInfo.ResourceId, SetupObject))
			{
				const TArray<FACUnrealResourceLocation>* const ModelLocations = SetupObject.PlacementFrames.Contains(0x65) ? SetupObject.PlacementFrames.Find(0x65) : SetupObject.PlacementFrames.Find(0);
				for (int32 ModelResourceIndex = 0; ModelResourceIndex < SetupObject.ModelResourceIds.Num(); ++ModelResourceIndex)
				{
					const int32 ModelResourceId = SetupObject.ModelResourceIds[ModelResourceIndex];

					FACUnrealResourceLocation ModelLocation;
					if (ModelLocations)
					{
						ModelLocation = (*ModelLocations)[(ModelResourceIndex < ModelLocations->Num()) ? ModelResourceIndex : 0];
					}
					else
					{
						ModelLocation.HeadingA = ModelLocation.HeadingB = ModelLocation.HeadingC = ModelLocation.HeadingW = ModelLocation.OffsetX = ModelLocation.OffsetY = ModelLocation.OffsetZ = 0.0f;
					}

					ModelLocation = SumResourceLocation(ModelLocation, ResourceInfo.Location);

					// todo: is SetupObject.Flags used for anything? i.e. scaling/placement frame/etc?
					// todo: model scales

					const FACUnrealResourceInfo SubResourceInfo(ModelResourceId, ModelLocation);
					const bool bSubResourceInParentsBounds = (FMath::Abs(SubResourceInfo.Location.OffsetX) < 96.0f) && (FMath::Abs(SubResourceInfo.Location.OffsetY) < 96.0f);
					OutContent.Append(FString(FACUnrealT3dLandblockStaticObject(SubResourceInfo, ObjectIndex, ModelResourceIndex, NextDepth + 2, TEXT("SceneComponent"), TEXT("DefaultSceneRoot"), bSubResourceInParentsBounds, false, NamePrefix)));
				}

				for (int32 LightIndex = 0; LightIndex < SetupObject.Lights.Num(); ++LightIndex)
				{
					FACUnrealResourceLocation LightLocation = SumResourceLocation(SetupObject.Lights[LightIndex].Location, ResourceInfo.Location);

					FACUnrealT3dLightComponent LightComponent(SetupObject.Lights[LightIndex], LightLocation, ObjectIndex + LightIndex, NextDepth + 2, TEXT("SceneComponent"), TEXT("DefaultSceneRoot"));
					OutContent.Append(FString(LightComponent));
				}
			}
		}
		break;

		case EACUnrealResource_Native::kScene:
		{
			FACUnrealPortalScene Scene;
			if (PortalFile.GetResource(ResourceInfo.ResourceId, Scene))
			{
				for (int32 SceneObjectIndex = 0; SceneObjectIndex < Scene.Objects.Num(); ++SceneObjectIndex)
				{
					const FACUnrealPortalSceneObject& SceneObject = Scene.Objects[SceneObjectIndex];
					AppendResourceBody(PortalFile, SceneObject.ResourceInfo, NamePrefix, ObjectIndex, NextDepth, OutContent);
				}
			}
		}
		break;
	}
}

int32 AppendResourceFooter(const FACUnrealPortalFile& PortalFile, const FACUnrealResourceInfo& ResourceInfo, const FString& NamePrefix, int32 ObjectIndex, int32 ResourceIndex, int32 NextDepth, FString& OutContent)
{
	switch (ResourceInfo.ResourceId & EACUnrealResource_Native::kMask)
	{
		case EACUnrealResource_Native::kEnvironment:
		case EACUnrealResource_Native::kModel:
			OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("InstanceComponents(%d)=StaticMeshComponent'\"%s%d_0x%08X\"'\r\n"), ResourceIndex++, *NamePrefix, ObjectIndex, ResourceInfo.ResourceId), NextDepth + 2)));
			break;

		case EACUnrealResource_Native::kSetup:
		{
			FACUnrealPortalSetup SetupObject;
			if (PortalFile.GetResource(ResourceInfo.ResourceId, SetupObject))
			{
				for (int32 ModelResourceIndex = 0; ModelResourceIndex < SetupObject.ModelResourceIds.Num(); ++ModelResourceIndex)
				{
					const int32 ModelResourceId = SetupObject.ModelResourceIds[ModelResourceIndex];
					OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("InstanceComponents(%d)=StaticMeshComponent'\"%s%d_0x%08X_%d\"'\r\n"), ResourceIndex++, *NamePrefix, ObjectIndex, ModelResourceId, ModelResourceIndex), NextDepth + 2)));
				}

				for (int32 LightIndex = 0; LightIndex < SetupObject.Lights.Num(); ++LightIndex)
				{
					OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("InstanceComponents(%d)=PointLightComponent'\"Light_%03d\"'\r\n"), ResourceIndex++, ObjectIndex + LightIndex), NextDepth + 2)));
				}
			}
		}
		break;

		case EACUnrealResource_Native::kScene:
		{
			FACUnrealPortalScene Scene;
			if (PortalFile.GetResource(ResourceInfo.ResourceId, Scene))
			{
				for (int32 SceneObjectIndex = 0; SceneObjectIndex < Scene.Objects.Num(); ++SceneObjectIndex)
				{
					const FACUnrealPortalSceneObject& SceneObject = Scene.Objects[SceneObjectIndex];
					AppendResourceFooter(PortalFile, SceneObject.ResourceInfo, NamePrefix, ObjectIndex, ResourceIndex, NextDepth, OutContent);
				}
			}
		}
		break;
	}

	return ResourceIndex;
}

struct FACUnrealT3dScene : public FACUnrealT3dSection
{
public:
	FACUnrealT3dScene(const FACUnrealPortalScene& InScene)
		: FACUnrealT3dSection(TEXT("Map"), TEXT(""), 0)
		, Scene(InScene)
	{
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		const FString NamePrefix = TEXT("");

		IACUnrealDatToolsModule& ACUnrealDatToolsModule = FModuleManager::LoadModuleChecked<IACUnrealDatToolsModule>("ACUnrealDatTools");
		FACUnrealPortalFile PortalFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Portal));

		FACUnrealT3dSection LevelSection(TEXT("Level"), TEXT(""), NextDepth);
		OutContent.Append(FString(LevelSection.GetContent()));

		FACUnrealT3dSection ActorSection(TEXT("Actor"), FString::Printf(TEXT(" Class=/Script/Engine.Actor Name=0x%08X Archetype=/Script/Engine.Actor'/Script/Engine.Default__Actor'"), Scene.ResourceId), NextDepth + 1);
		OutContent.Append(FString(ActorSection.GetContent()));

		// resource headers
		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Object"), FString::Printf(TEXT(" Class=/Script/Engine.SceneComponent Name=\"DefaultSceneRoot\"")), NextDepth + 2)));

		for (int32 ObjectIndex = 0; ObjectIndex < Scene.Objects.Num(); ++ObjectIndex)
		{
			const FACUnrealResourceInfo& StaticObject = Scene.Objects[ObjectIndex].ResourceInfo;
			AppendResourceHeader(PortalFile, StaticObject, NamePrefix, ObjectIndex, NextDepth, OutContent);
		}

		// resource body
		for (int32 ObjectIndex = 0; ObjectIndex < Scene.Objects.Num(); ++ObjectIndex)
		{
			const FACUnrealResourceInfo& StaticObject = Scene.Objects[ObjectIndex].ResourceInfo;
			AppendResourceBody(PortalFile, StaticObject, NamePrefix, ObjectIndex, NextDepth, OutContent);
		}

		FACUnrealT3dSection SceneRootSection(TEXT("Object"), TEXT(" Name=\"DefaultSceneRoot\""), NextDepth + 2);
		OutContent.Append(FString(SceneRootSection.GetContent()));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("RelativeLocation=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), 0.0f, 0.0f, 0.0f), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bVisualizeComponent=True\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CreationMethod=Instance\r\n"), NextDepth + 3)));
		SceneRootSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("RootComponent=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("ActorLabel=\"0x%08X\"\r\n"), Scene.ResourceId), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("InstanceComponents(0)=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));

		int32 ResourceIndex = 1;

		// resource footers
		for (int32 ObjectIndex = 0; ObjectIndex < Scene.Objects.Num(); ++ObjectIndex)
		{
			const FACUnrealResourceInfo& StaticObject = Scene.Objects[ObjectIndex].ResourceInfo;
			ResourceIndex = AppendResourceFooter(PortalFile, StaticObject, NamePrefix, ObjectIndex, ResourceIndex, NextDepth, OutContent);
		}

		ActorSection.AppendFooter(OutContent);
		LevelSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Surface"), TEXT(""), TabDepth)));
	}

	FACUnrealPortalScene Scene;
};

struct FACUnrealT3dSetup : public FACUnrealT3dSection
{
public:
	FACUnrealT3dSetup(const FACUnrealPortalSetup& InSetup)
		: FACUnrealT3dSection(TEXT("Map"), TEXT(""), 0)
		, Setup(InSetup)
	{
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		const FString NamePrefix = TEXT("");

		IACUnrealDatToolsModule& ACUnrealDatToolsModule = FModuleManager::LoadModuleChecked<IACUnrealDatToolsModule>("ACUnrealDatTools");
		FACUnrealPortalFile PortalFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Portal));

		FBox3f SetupBounds = FBox3f::BuildAABB(FVector3f::ZeroVector, FVector3f(10.0f));

		// resource headers
		FACUnrealT3dSection LevelSection(TEXT("Level"), TEXT(""), NextDepth);
		OutContent.Append(FString(LevelSection.GetContent()));

		FACUnrealT3dSection ActorSection(TEXT("Actor"), FString::Printf(TEXT(" Class=/Script/Engine.Actor Name=0x%08X Archetype=/Script/Engine.Actor'/Script/Engine.Default__Actor'"), Setup.ResourceId), NextDepth + 1);
		OutContent.Append(FString(ActorSection.GetContent()));

		FACUnrealT3dSection BoundsBoxRootSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BoxComponent Name=\"DefaultSceneRoot\""), NextDepth + 2);
		OutContent.Append(FString(BoundsBoxRootSection.GetContent()));
		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BodySetup Name=\"BodySetup_0\""), NextDepth + 3)));
		BoundsBoxRootSection.AppendFooter(OutContent);

		for (int32 ModelResourceIndex = 0; ModelResourceIndex < Setup.ModelResourceIds.Num(); ++ModelResourceIndex)
		{
			FACUnrealResourceInfo ModelResourceInfo;
			ModelResourceInfo.ResourceId = Setup.ModelResourceIds[ModelResourceIndex];
			AppendResourceHeader(PortalFile, ModelResourceInfo, NamePrefix, ModelResourceIndex, NextDepth, OutContent);
		}

		// resource body
		{
			const TArray<FACUnrealResourceLocation>* const ModelLocations = Setup.PlacementFrames.Contains(0x65) ? Setup.PlacementFrames.Find(0x65) : Setup.PlacementFrames.Find(0);
			for (int32 ModelResourceIndex = 0; ModelResourceIndex < Setup.ModelResourceIds.Num(); ++ModelResourceIndex)
			{
				const int32 ModelResourceId = Setup.ModelResourceIds[ModelResourceIndex];

				FACUnrealResourceLocation ModelLocation;
				if (ModelLocations)
				{
					ModelLocation = (*ModelLocations)[(ModelResourceIndex < ModelLocations->Num()) ? ModelResourceIndex : 0];
				}
				else
				{
					ModelLocation.HeadingA = ModelLocation.HeadingB = ModelLocation.HeadingC = ModelLocation.HeadingW = ModelLocation.OffsetX = ModelLocation.OffsetY = ModelLocation.OffsetZ = 0.0f;
				}

				// todo: is SetupObject.Flags used for anything? i.e. scaling/placement frame/etc?
				// todo: model scales

				const FACUnrealResourceInfo ResourceInfo(ModelResourceId, ModelLocation);
				FACUnrealPortalModel ResourceModel;
				if (PortalFile.GetResource(ModelResourceId, ResourceModel))
				{
					for (const FACUnrealPortalModelVertex& ModelVertex : ResourceModel.Vertices)
					{
						FACUnrealResourceLocation VertexLocation;
						VertexLocation.OffsetX = ModelVertex.Position.X;
						VertexLocation.OffsetY = ModelVertex.Position.Y;
						VertexLocation.OffsetZ = ModelVertex.Position.Z;
						FACUnrealResourceLocation ModelVertexLocation = SumResourceLocation(ModelLocation, VertexLocation);

						const FVector3f ModelVertexLocationVector = FVector3f(ModelVertexLocation.OffsetX, ModelVertexLocation.OffsetY, ModelVertexLocation.OffsetZ);
						SetupBounds = SetupBounds.ExpandBy(ModelVertexLocationVector);
					}
				}

				AppendResourceBody(PortalFile, ResourceInfo, NamePrefix, ModelResourceIndex, NextDepth, OutContent);
			}
		}

		FACUnrealT3dSection SceneRootSection(TEXT("Object"), TEXT(" Name=\"DefaultSceneRoot\""), NextDepth + 2);
		OutContent.Append(FString(SceneRootSection.GetContent()));

		FACUnrealT3dSection SceneRootBodySetupSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BodySetup Name=\"BodySetup_0\""), NextDepth + 3);
		OutContent.Append(FString(SceneRootBodySetupSection.GetContent()));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("AggGeom=(BoxElems=((TM=(XPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=-0.000000),YPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=0.000000),ZPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=-0.000000),WPlane=(W=0.000000,X=-76802848.000000,Y=0.000000,Z=0.000000)),X=19200.000000,Y=19200.000000,Z=25600.000000)))\r\n"), NextDepth + 4)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CollisionTraceFlag=CTF_UseSimpleAsComplex\r\n"), NextDepth + 4)));
		SceneRootBodySetupSection.AppendFooter(OutContent);

		// todo: SetupBounds is wrong and is would need to be centered to be correct
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("BoxExtent=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), SetupBounds.GetExtent().X, SetupBounds.GetExtent().Y, SetupBounds.GetExtent().Z), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bGenerateOverlapEvents=False\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CanCharacterStepUpOn=ECB_No\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("BodyInstance=(ObjectType=ECC_WorldStatic,CollisionProfileName=\"NoCollision\",CollisionResponses=(ResponseArray=((Channel=\"Visibility\",Response=ECR_Ignore),(Channel=\"Camera\",Response=ECR_Ignore))),CollisionEnabled=NoCollision,MaxAngularVelocity=3599.999756)\r\n"), NextDepth + 3)));

		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("RelativeLocation=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), 0.0f, 0.0f, 0.0f), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bVisualizeComponent=True\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CreationMethod=Instance\r\n"), NextDepth + 3)));
		SceneRootSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("RootComponent=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("ActorLabel=\"0x%08X\"\r\n"), Setup.ResourceId), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("InstanceComponents(0)=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));

		int32 ResourceIndex = 1;
		// resource footers
		for (int32 ModelResourceIndex = 0; ModelResourceIndex < Setup.ModelResourceIds.Num(); ++ModelResourceIndex)
		{
			FACUnrealResourceInfo ModelResourceInfo;
			ModelResourceInfo.ResourceId = Setup.ModelResourceIds[ModelResourceIndex];
			ResourceIndex = AppendResourceFooter(PortalFile, ModelResourceInfo, NamePrefix, ModelResourceIndex, ResourceIndex, NextDepth, OutContent);
		}

		ActorSection.AppendFooter(OutContent);
		LevelSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Surface"), TEXT(""), TabDepth)));
	}

	FACUnrealPortalSetup Setup;
};

double prng(uint32 CellX, uint32 CellY, uint32 Seed)
{
	uint32 Ival = 0x6C1AC587 * CellY - 0x421BE3BD * CellX - Seed * (0x5111BFEF * CellY * CellX + 0x70892FB7);
	return static_cast<double>(Ival) * 2.3283064e-10;
}

struct FACUnrealScaledResourceInfo
{
	//
	uint32 ResourceId;
	//
	FACUnrealResourceLocation Location;
	//
	float Scale = 1.0f;
};

// TODO: Scale needs to be returned from here
//TArray<FACUnrealScaledResourceInfo> GetLandblockSceneResources(const FACUnrealPortalRegion& Region, uint32 CellX, uint32 CellY)
TResourceInfoArray GetLandblockSceneResources(const FACUnrealPortalRegion& Region, uint32 CellX, uint32 CellY)
{
	//TArray<FACUnrealScaledResourceInfo> SceneResources;
	TResourceInfoArray SceneResources;

	IACUnrealDatToolsModule& ACUnrealDatToolsModule = FModuleManager::LoadModuleChecked<IACUnrealDatToolsModule>("ACUnrealDatTools");
	FACUnrealPortalFile PortalFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Portal));
	FACUnrealCellFile CellFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Cell));

	const FACUnrealCellLandblock* const LandBlock = CellFile.GetLandblock(CellX, CellY);
	if (LandBlock)
	{
		FACUnrealCellLandblockObject lbi;
		CellFile.GetLandblockObject(CellX, CellY, lbi);

		for (uint32 TopographyY = 0; TopographyY < FACUnrealCellLandblock::kDimension; ++TopographyY)
		{
			for (uint32 TopographyX = 0; TopographyX < FACUnrealCellLandblock::kDimension; ++TopographyX)
			{
				const uint8 RegionTerrain = LandBlock->GetRegionTerrain(TopographyX, TopographyY);
				check(RegionTerrain < static_cast<uint32>(Region.TerrainTypes.Num()));
				const uint8 RegionTerrainScene = LandBlock->GetRegionScene(TopographyX, TopographyY);
				check(RegionTerrainScene < static_cast<uint32>(Region.TerrainTypes[RegionTerrain].TerrainSceneTypes.Num()));
				const uint32 SceneType = Region.TerrainTypes[RegionTerrain].TerrainSceneTypes[RegionTerrainScene];
				check(SceneType < static_cast<uint32>(Region.SceneTypes.Num()));

				const uint32 SceneIdCount = Region.SceneTypes[SceneType].SceneIds.Num();
				if (0 < SceneIdCount)
				{
					// TODO: use global var/const for lblock_side (8)
					uint32 SceneX = CellX * 8 + TopographyX;
					uint32 SceneY = CellY * 8 + TopographyY;

					uint32 SceneNum = static_cast<uint32>(prng(SceneX, SceneY, 0x00002bf9) * static_cast<double>(SceneIdCount));

					if (SceneNum >= SceneIdCount)
					{
						SceneNum = 0;
					}

					const uint32 SceneID = Region.SceneTypes[SceneType].SceneIds[SceneNum];
					FACUnrealPortalScene Scene;
					const bool ValidScene = PortalFile.GetResource(SceneID, Scene);
					check(ValidScene);

					for (int32 i = 0; i < Scene.Objects.Num(); i++)
					{
						const FACUnrealPortalSceneObject& PortalSceneObject = Scene.Objects[i];
						if (PortalSceneObject.IntIsWeenieObj != 0)
						{
							//PortalSceneObject.ResourceInfo.ResourceId is invalid?
							continue;
						}

						if (!(prng(SceneX, SceneY, 0x00005b67/* + i*/) < PortalSceneObject.Frequency))
						{
							// hidden
							continue;
						}

						FVector3f CellPos = FVector3f(PortalSceneObject.ResourceInfo.Location.OffsetX, PortalSceneObject.ResourceInfo.Location.OffsetY, PortalSceneObject.ResourceInfo.Location.OffsetZ);

						if (PortalSceneObject.DisplaceX > 0.0f)
						{
							CellPos.X += static_cast<float>(prng(SceneX, SceneY, 0x0000b2cd + i) * PortalSceneObject.DisplaceX);
						}

						if (PortalSceneObject.DisplaceY > 0.0f)
						{
							CellPos.Y += static_cast<float>(prng(SceneX, SceneY, 0x00011c0f + i) * PortalSceneObject.DisplaceY);
						}

						FVector3f TempPos = CellPos;

						double sceneRot = prng(SceneX, SceneY, 0x0000e7eb);

						if (sceneRot >= 0.75)
						{
							CellPos.X = TempPos.Y;
							CellPos.Y = -TempPos.X;
						}
						else if (sceneRot >= 0.5)
						{
							CellPos.X = -TempPos.X;
							CellPos.Y = -TempPos.Y;
						}
						else if (sceneRot >= 0.25)
						{
							CellPos.X = -TempPos.Y;
							CellPos.Y = TempPos.X;
						}
						else
						{
							CellPos.X = TempPos.Y;
							CellPos.Y = TempPos.X;
						}

						FVector3f BlockPos = CellPos + FVector3f(TopographyX * 24.0f, TopographyY * 24.0f, 0.0);

						if (BlockPos.X < 0.0 || BlockPos.X >= 192.0f || BlockPos.Y < 0.0 || BlockPos.Y >= 192.0f)
						{
							continue;
						}

						if (LandBlock->OnRoad(BlockPos, Region))
							continue;

						if (lbi.StaticObjectExCount > 0)
						{
							uint32 objcellid = ((uint32)(floor(BlockPos.X / 24.0f)) << 3) | (uint32)(floor(BlockPos.Y / 24.0f));

							bool found = false;
							for (int32 bi = 0; bi < lbi.StaticObjectExCount; bi++)
							{
								const FACUnrealResourceLocation& loc = lbi.StaticObjectsEx[bi].Location;
								uint32 bldcellid = ((uint32)(floor(loc.OffsetX / 24.0f)) << 3) | (uint32)(floor(loc.OffsetY / 24.0f));

								if (objcellid == bldcellid)
								{
									found = true;
									break;
								}
							}

							if (found) continue;
						}

						const FPlane4f TrianglePlane = LandBlock->GetTrianglePlane(BlockPos.X, BlockPos.Y, Region.HeightValues);

						float Dot = FVector3f::DotProduct(FVector3f::UpVector, TrianglePlane);
						float Radians = FMath::Acos(Dot);
						float Measurement = TrianglePlane.Z;
						if (Measurement < PortalSceneObject.MinSlope || Measurement > PortalSceneObject.MaxSlope)
						{
							continue;
						}

						BlockPos.Z += LandBlock->GetHeight(BlockPos.X, BlockPos.Y, Region.HeightValues);

						const float Scale = (PortalSceneObject.MinScale == PortalSceneObject.MaxScale) ? PortalSceneObject.MaxScale : static_cast<float>(PortalSceneObject.MinScale * FMath::Pow(PortalSceneObject.MaxScale / PortalSceneObject.MinScale, prng(SceneX, SceneY, 0x00007f51 + i)));

						const float RandYaw = FMath::DegreesToRadians(static_cast<float>(prng(SceneX, SceneY, 0x0000f697 + i)) * PortalSceneObject.MaxRotation * 0.0174533f);

						//FACUnrealScaledResourceInfo ResourceInfo;
						FACUnrealResourceInfo ResourceInfo;
						ResourceInfo.ResourceId = PortalSceneObject.ResourceInfo.ResourceId;
						ResourceInfo.Location.OffsetX = BlockPos.X;
						ResourceInfo.Location.OffsetY = BlockPos.Y;
						ResourceInfo.Location.OffsetZ = BlockPos.Z;
						ResourceInfo.Location.HeadingA = 0.0f;
						ResourceInfo.Location.HeadingB = 0.0f;
						ResourceInfo.Location.HeadingC = 1.0f;
						ResourceInfo.Location.HeadingW = RandYaw;
						//ResourceInfo.Scale = Scale;
						SceneResources.Add(ResourceInfo);
					}
				}
			}
		}
	}

	return SceneResources;
}

FBox3f GetLandblockBounds(const FACUnrealPortalRegion& Region, uint32 CellX, uint32 CellY)
{
	TArray<FACUnrealResourceInfo> SceneResources;

	IACUnrealDatToolsModule& ACUnrealDatToolsModule = FModuleManager::LoadModuleChecked<IACUnrealDatToolsModule>("ACUnrealDatTools");
	FACUnrealPortalFile PortalFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Portal));
	FACUnrealCellFile CellFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Cell));

	float High = 50.0f;
	float Low = -50.0f;

	const FACUnrealCellLandblock* const LandBlock = CellFile.GetLandblock(CellX, CellY);
	if (LandBlock)
	{
		for (uint32 TopographyY = 0; TopographyY < FACUnrealCellLandblock::kDimension; ++TopographyY)
		{
			for (uint32 TopographyX = 0; TopographyX < FACUnrealCellLandblock::kDimension; ++TopographyX)
			{
				const float RegionTopographyHeight = 128.0f * LandBlock->GetHeightAt(TopographyX, TopographyY, Region.HeightValues);
				if (RegionTopographyHeight < Low)
				{
					Low = RegionTopographyHeight;
				}

				if (High < RegionTopographyHeight)
				{
					High = RegionTopographyHeight;
				}
			}
		}
	}

	const float MidPoint = Low + (FMath::Abs(Low) + FMath::Abs(High) * 0.5f);

	return FBox3f::BuildAABB(FVector3f(0.0f, 0.0f, /*MidPoint*/0.0f), FVector3f(9600.0f, 9600.0f, High));
}

void MapResources(const TResourceInfoArray& Resources, TResourceLocationsMap& InOutResourceMap)
{
	IACUnrealDatToolsModule& ACUnrealDatToolsModule = FModuleManager::LoadModuleChecked<IACUnrealDatToolsModule>("ACUnrealDatTools");
	FACUnrealPortalFile PortalFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Portal));

	//FACUnrealScaledResourceInfo
	for (const FACUnrealResourceInfo& Resource : Resources)
	{
		if (EACUnrealResource_Native::kSetup == (Resource.ResourceId & EACUnrealResource_Native::kMask))
		{
			FACUnrealPortalSetup SetupObject;
			if (PortalFile.GetResource(Resource.ResourceId, SetupObject))
			{
				const TArray<FACUnrealResourceLocation>* const ModelLocations = SetupObject.PlacementFrames.Contains(0x65) ? SetupObject.PlacementFrames.Find(0x65) : SetupObject.PlacementFrames.Find(0);
				for (int32 ModelResourceIndex = 0; ModelResourceIndex < SetupObject.ModelResourceIds.Num(); ++ModelResourceIndex)
				{
					const int32 ModelResourceId = SetupObject.ModelResourceIds[ModelResourceIndex];

					FACUnrealResourceLocation ModelLocation;
					if (ModelLocations)
					{
						ModelLocation = (*ModelLocations)[(ModelResourceIndex < ModelLocations->Num()) ? ModelResourceIndex : 0];
					}
					else
					{
						ModelLocation.HeadingA = ModelLocation.HeadingB = ModelLocation.HeadingC = ModelLocation.HeadingW = ModelLocation.OffsetX = ModelLocation.OffsetY = ModelLocation.OffsetZ = 0.0f;
					}

					ModelLocation = SumResourceLocation(ModelLocation, Resource.Location);

					if (!InOutResourceMap.Contains(ModelResourceId))
					{
						InOutResourceMap.Add(ModelResourceId, MakeShareable(new TArray<FACUnrealResourceLocation>()));
					}

					InOutResourceMap[ModelResourceId]->Add(ModelLocation);
				}
			}
		}
		else
		{
			check(EACUnrealResource_Native::kModel == (Resource.ResourceId & EACUnrealResource_Native::kMask));
			if (!InOutResourceMap.Contains(Resource.ResourceId))
			{
				InOutResourceMap.Add(Resource.ResourceId, MakeShareable(new TArray<FACUnrealResourceLocation>()));
			}

			InOutResourceMap[Resource.ResourceId]->Add(Resource.Location);
		}
	}
}

struct FACUnrealT3dMap : public FACUnrealT3dSection
{
public:
	FACUnrealT3dMap(const int32 InCellX, const int32 InCellY, const FACUnrealCellLandblockObject& InLandblockInfos)
	: FACUnrealT3dSection(TEXT("Map"), TEXT(""), 0)
	, LandblockInfos(InLandblockInfos)
	, CellX(InCellX)
	, CellY(InCellY)
	{
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		const FString NamePrefixEnvironment = TEXT("ev");
		const FString NamePrefixStatic = TEXT("sa");
		const FString NamePrefixStaticEx = TEXT("se");
		const FString NamePrefixStructure = TEXT("su");
		const FString NamePrefixScene = TEXT("sc");

		IACUnrealDatToolsModule& ACUnrealDatToolsModule = FModuleManager::LoadModuleChecked<IACUnrealDatToolsModule>("ACUnrealDatTools");
		FACUnrealPortalFile PortalFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Portal));
		FACUnrealCellFile CellFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Cell));

		FACUnrealPortalRegion PortalRegion;
		const bool ValidRegion = PortalFile.GetResource(EACUnrealResource_Native::kRegion, PortalRegion);
		check(ValidRegion);

		TArray<FACUnrealCellStructure> Structures;
		if (0 < LandblockInfos.StructureCount)
		{
			CellFile.GetLandblockStructures(LandblockInfos, Structures);
		}

		FBox3f Bounds = GetLandblockBounds(PortalRegion, CellX, CellY);

		FACUnrealT3dSection LevelSection(TEXT("Level"), TEXT(""), NextDepth);
		OutContent.Append(FString(LevelSection.GetContent()));

		FACUnrealT3dSection ActorSection(TEXT("Actor"), FString::Printf(TEXT(" Class=/Script/Engine.Actor Name=Landblock_%03d_%03d Archetype=/Script/Engine.Actor'/Script/Engine.Default__Actor'"), CellX, CellY), NextDepth + 1);
		OutContent.Append(FString(ActorSection.GetContent()));

		TResourceLocationsMap StructureMap;
		for (const FACUnrealCellStructure& Structure : Structures)
		{
			for (const FACUnrealResourceInfo& StaticObjectEs : Structure.StaticObjects)
			{
				MapResources(Structure.StaticObjects, StructureMap);
			}
		}

		TArray<uint32> StructureMapKeys;
		const int32 NumStructureKeys = StructureMap.GetKeys(StructureMapKeys);

		// resource headers
		FACUnrealT3dSection BoundsBoxRootSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BoxComponent Name=\"DefaultSceneRoot\""), NextDepth + 2);
		OutContent.Append(FString(BoundsBoxRootSection.GetContent()));
		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BodySetup Name=\"BodySetup_0\""), NextDepth + 3)));
		BoundsBoxRootSection.AppendFooter(OutContent);

		{
			for (int32 StaticObjectIndex = 0; StaticObjectIndex < LandblockInfos.StaticObjectCount; ++StaticObjectIndex)
			{
				const FACUnrealResourceInfo& StaticObject = LandblockInfos.StaticObjects[StaticObjectIndex];
				AppendResourceHeader(PortalFile, StaticObject, NamePrefixStatic, StaticObjectIndex, NextDepth, OutContent);
				Bounds += ToUnrealLocation(StaticObject.Location);
			}

			for (int32 StaticObjectIndexEx = 0; StaticObjectIndexEx < LandblockInfos.StaticObjectExCount; ++StaticObjectIndexEx)
			{
				const FACUnrealResourceInfo& StaticObjectEx = LandblockInfos.StaticObjectsEx[StaticObjectIndexEx];
				AppendResourceHeader(PortalFile, StaticObjectEx, NamePrefixStaticEx, StaticObjectIndexEx, NextDepth, OutContent);
				Bounds += ToUnrealLocation(StaticObjectEx.Location);
			}

			int32 StructureObjectIndex = 0;
			for (uint32 Key : StructureMapKeys)
			{
				if (1 < StructureMap[Key]->Num())
				{
					FACUnrealT3dSection InstancedMeshSection(TEXT("Object"), FString::Printf(TEXT(" Class=/Script/Engine.InstancedStaticMeshComponent Name=\"%s%d_0x%08X_Instanced\""), *NamePrefixStructure, StructureObjectIndex, Key), NextDepth + 2);
					OutContent.Append(FString(InstancedMeshSection.GetContent()));
					InstancedMeshSection.AppendBody(OutContent);
					InstancedMeshSection.AppendFooter(OutContent);
				}
				else
				{
					FACUnrealResourceInfo StaticObject(Key, (*StructureMap[Key])[0]);
					AppendResourceHeader(PortalFile, StaticObject, NamePrefixStructure, StructureObjectIndex, NextDepth, OutContent);
				}

				StructureObjectIndex++;
			}

			TMap<uint32, TSet<FVector3f>> EnvironmentMap;

			for (int32 StructureIndex = 0; StructureIndex < Structures.Num(); ++StructureIndex)
			{
				const FACUnrealCellStructure& Structure = Structures[StructureIndex];
				TSet<FVector3f>* Locations = EnvironmentMap.Find(Structure.EnvironmentId);
				const FVector3f Location(Structure.Location.OffsetX, -Structure.Location.OffsetY, Structure.Location.OffsetZ);
				if (0 != Structure.EnvironmentId && (!Locations || !Locations->Contains(Location)))
				{
					FACUnrealResourceInfo StructureEnvironment;
					StructureEnvironment.Location = Structure.Location;
					StructureEnvironment.ResourceId = EACUnrealResource_Native::kEnvironment | Structure.EnvironmentId;
					AppendResourceHeader(PortalFile, StructureEnvironment, NamePrefixEnvironment, StructureIndex, NextDepth, OutContent);

					if (Locations)
					{
						Locations->Add(Location);
					}
					else
					{
						TSet<FVector3f> NewLocations;
						NewLocations.Add(Location);
						EnvironmentMap.Add(Structure.EnvironmentId, NewLocations);
					}

					Bounds += ToUnrealLocation(StructureEnvironment.Location);
				}
			}
		}

		// resource bodies
		{
			for (int32 StaticObjectIndex = 0; StaticObjectIndex < LandblockInfos.StaticObjectCount; ++StaticObjectIndex)
			{
				FACUnrealResourceInfo StaticObject = LandblockInfos.StaticObjects[StaticObjectIndex];
				StaticObject.Location.OffsetX -= 96.0f;
				StaticObject.Location.OffsetY -= 96.0f;
				AppendResourceBody(PortalFile, StaticObject, NamePrefixStatic, StaticObjectIndex, NextDepth, OutContent);
			}

			for (int32 StaticObjectIndexEx = 0; StaticObjectIndexEx < LandblockInfos.StaticObjectExCount; ++StaticObjectIndexEx)
			{
				FACUnrealResourceInfo StaticObjectEx = LandblockInfos.StaticObjectsEx[StaticObjectIndexEx];
				StaticObjectEx.Location.OffsetX -= 96.0f;
				StaticObjectEx.Location.OffsetY -= 96.0f;
				StaticObjectEx.Location.OffsetZ += kStructureExEnvironmentZBump;
				AppendResourceBody(PortalFile, StaticObjectEx, NamePrefixStaticEx, StaticObjectIndexEx, NextDepth, OutContent);
			}

			int32 StructureObjectIndex = 0;
			for (uint32 Key : StructureMapKeys)
			{
				const TSharedPtr<TArray<FACUnrealResourceLocation>>& Locations = StructureMap[Key];
				if (1 < StructureMap[Key]->Num())
				{
					FACUnrealT3dLandblockStaticObjectInstanced InstancedObject(Key, StructureObjectIndex, NextDepth + 2, TEXT("BoxComponent"), TEXT("DefaultSceneRoot"), false, false, NamePrefixStructure);

					for (int32 InstanceIndex = 0; InstanceIndex < Locations->Num(); ++InstanceIndex)
					{
						FACUnrealResourceLocation ObjectLocation = (*Locations)[InstanceIndex];
						ObjectLocation.OffsetX -= 96.0f;
						ObjectLocation.OffsetY -= 96.0f;
						ObjectLocation.OffsetZ += kStructureExEnvironmentZBump;
						InstancedObject.AddInstance(ObjectLocation);
					}

					OutContent.Append(FString(InstancedObject.GetContent()));
					InstancedObject.AppendPerInstanceData(OutContent);
					InstancedObject.AppendBody(OutContent);
					InstancedObject.AppendFooter(OutContent);
				}
				else
				{
					FACUnrealResourceInfo StaticObjectEs(Key, (*Locations)[0]);
					StaticObjectEs.Location.OffsetX -= 96.0f;
					StaticObjectEs.Location.OffsetY -= 96.0f;
					StaticObjectEs.Location.OffsetZ += kStructureExEnvironmentZBump;
					AppendResourceBody(PortalFile, StaticObjectEs, NamePrefixStructure, StructureObjectIndex, NextDepth, OutContent);
				}

				StructureObjectIndex++;
			}

			TMap<uint32, TSet<FVector3f>> EnvironmentMap;

			for (int32 StructureIndex = 0; StructureIndex < Structures.Num(); ++StructureIndex)
			{
				const FACUnrealCellStructure& Structure = Structures[StructureIndex];
				TSet<FVector3f>* Locations = EnvironmentMap.Find(Structure.EnvironmentId);
				const FVector3f Location(Structure.Location.OffsetX, -Structure.Location.OffsetY, Structure.Location.OffsetZ);
				if (0 != Structure.EnvironmentId && (!Locations || !Locations->Contains(Location)))
				{
					FACUnrealResourceInfo StructureEnvironment;
					StructureEnvironment.Location = Structure.Location;
					StructureEnvironment.Location.OffsetX -= 96.0f;
					StructureEnvironment.Location.OffsetY -= 96.0f;
					StructureEnvironment.Location.OffsetZ += kStructureExEnvironmentZBump;
					StructureEnvironment.ResourceId = EACUnrealResource_Native::kEnvironment | Structure.EnvironmentId;

					TArray<uint32> SurfaceIds;

					FACUnrealPortalEnvironment Environment;
					const bool EnvironmentOk = PortalFile.GetResource(StructureEnvironment.ResourceId, Environment);
					if (EnvironmentOk)
					{
						for (uint16 SurfaceIndex : Environment.GetPartSurfaceIndices(true))
						{
							check(SurfaceIndex < static_cast<uint32>(Structure.SurfaceIDs.Num()));
							SurfaceIds.Add(Structure.SurfaceIDs[SurfaceIndex]);
						}
					}

					AppendResourceBody(PortalFile, StructureEnvironment, NamePrefixEnvironment, StructureIndex, NextDepth, OutContent, SurfaceIds);

					if (Locations)
					{
						Locations->Add(Location);
					}
					else
					{
						TSet<FVector3f> NewLocations;
						NewLocations.Add(Location);
						EnvironmentMap.Add(Structure.EnvironmentId, NewLocations);
					}
				}
			}
		}

		FACUnrealT3dSection SceneRootSection(TEXT("Object"), TEXT(" Name=\"DefaultSceneRoot\""), NextDepth + 2);
		OutContent.Append(FString(SceneRootSection.GetContent()));

		FACUnrealT3dSection SceneRootBodySetupSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BodySetup Name=\"BodySetup_0\""), NextDepth + 3);
		OutContent.Append(FString(SceneRootBodySetupSection.GetContent()));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("AggGeom=(BoxElems=((TM=(XPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=-0.000000),YPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=0.000000),ZPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=-0.000000),WPlane=(W=0.000000,X=-76802848.000000,Y=0.000000,Z=0.000000)),X=19200.000000,Y=19200.000000,Z=25600.000000)))\r\n"), NextDepth + 4)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CollisionTraceFlag=CTF_UseSimpleAsComplex\r\n"), NextDepth + 4)));
		SceneRootBodySetupSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("BoxExtent=(X=9600.000000,Y=9600.000000,Z=%1.6f)\r\n"), Bounds.GetExtent().Z), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bGenerateOverlapEvents=False\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CanCharacterStepUpOn=ECB_No\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("BodyInstance=(ObjectType=ECC_WorldStatic,CollisionProfileName=\"NoCollision\",CollisionResponses=(ResponseArray=((Channel=\"Visibility\",Response=ECR_Ignore),(Channel=\"Camera\",Response=ECR_Ignore))),CollisionEnabled=NoCollision,MaxAngularVelocity=3599.999756)\r\n"), NextDepth + 3)));

		const float LandblockX = static_cast<float>(CellX - 127) * 19200.0f;
		const float LandblockY = static_cast<float>(127 - CellY) * 19200.0f;
		const float LandblockZ = 0.0f;
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("RelativeLocation=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), LandblockX, LandblockY, LandblockZ), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bVisualizeComponent=True\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CreationMethod=Instance\r\n"), NextDepth + 3)));
		SceneRootSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("RootComponent=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("ActorLabel=\"Landblock_%03d_%03d\"\r\n"), CellX, CellY), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("InstanceComponents(0)=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));

		int32 ResourceIndex = 1;
		// resource footers
		{
			for (int32 StaticObjectIndex = 0; StaticObjectIndex < LandblockInfos.StaticObjectCount; ++StaticObjectIndex)
			{
				const FACUnrealResourceInfo& StaticObject = LandblockInfos.StaticObjects[StaticObjectIndex];
				ResourceIndex = AppendResourceFooter(PortalFile, StaticObject, NamePrefixStatic, StaticObjectIndex, ResourceIndex, NextDepth, OutContent);
			}

			for (int32 StaticObjectIndexEx = 0; StaticObjectIndexEx < LandblockInfos.StaticObjectExCount; ++StaticObjectIndexEx)
			{
				const FACUnrealResourceInfo& StaticObjectEx = LandblockInfos.StaticObjectsEx[StaticObjectIndexEx];
				ResourceIndex = AppendResourceFooter(PortalFile, StaticObjectEx, NamePrefixStaticEx, StaticObjectIndexEx, ResourceIndex, NextDepth, OutContent);
			}

			int32 StructureObjectIndex = 0;
			for (uint32 Key : StructureMapKeys)
			{
				if (1 < StructureMap[Key]->Num())
				{
					OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("InstanceComponents(%d)=InstancedStaticMeshComponent'\"%s%d_0x%08X_Instanced\"'\r\n"), ResourceIndex++, *NamePrefixStructure, StructureObjectIndex, Key), NextDepth + 2)));
				}
				else
				{
					FACUnrealResourceInfo StaticObject(Key, (*StructureMap[Key])[0]);
					ResourceIndex = AppendResourceFooter(PortalFile, StaticObject, NamePrefixStructure, StructureObjectIndex, ResourceIndex, NextDepth, OutContent);
				}

				StructureObjectIndex++;
			}

			TMap<uint32, TSet<FVector3f>> EnvironmentMap;

			for (int32 StructureIndex = 0; StructureIndex < Structures.Num(); ++StructureIndex)
			{
				const FACUnrealCellStructure& Structure = Structures[StructureIndex];
				TSet<FVector3f>* Locations = EnvironmentMap.Find(Structure.EnvironmentId);
				const FVector3f Location(Structure.Location.OffsetX, -Structure.Location.OffsetY, Structure.Location.OffsetZ);
				if (0 != Structure.EnvironmentId && (!Locations || !Locations->Contains(Location)))
				{
					FACUnrealResourceInfo StructureEnvironment;
					StructureEnvironment.Location = Structure.Location;
					StructureEnvironment.ResourceId = EACUnrealResource_Native::kEnvironment | Structure.EnvironmentId;
					ResourceIndex = AppendResourceFooter(PortalFile, StructureEnvironment, NamePrefixEnvironment, StructureIndex, ResourceIndex, NextDepth, OutContent);

					if (Locations)
					{
						Locations->Add(Location);
					}
					else
					{
						TSet<FVector3f> NewLocations;
						NewLocations.Add(Location);
						EnvironmentMap.Add(Structure.EnvironmentId, NewLocations);
					}
				}
			}
		}

		ActorSection.AppendFooter(OutContent);
		LevelSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Surface"), TEXT(""), TabDepth)));
	}

	FACUnrealCellLandblockObject LandblockInfos;
	int32 CellX;
	int32 CellY;
};

struct FACUnrealT3dMapScene : public FACUnrealT3dSection
{
public:
	FACUnrealT3dMapScene(const int32 InStartX, const int32 InStartY, const int32 InCountX, const int32 InCountY)
	: FACUnrealT3dSection(TEXT("Map"), TEXT(""), 0)
	, StartX(InStartX)
	, StartY(InStartY)
	, CountX(InCountX)
	, CountY(InCountY)
	{
	}

	virtual void AppendBody(FString& OutContent) const override
	{
		const FString NamePrefixScene = TEXT("sc");

		IACUnrealDatToolsModule& ACUnrealDatToolsModule = FModuleManager::LoadModuleChecked<IACUnrealDatToolsModule>("ACUnrealDatTools");
		FACUnrealPortalFile PortalFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Portal));
		FACUnrealCellFile CellFile(ACUnrealDatToolsModule.GetDatFile(EACUnrealDatFile::Cell));

		FACUnrealPortalRegion PortalRegion;
		const bool ValidRegion = PortalFile.GetResource(EACUnrealResource_Native::kRegion, PortalRegion);
		check(ValidRegion);

		TResourceLocationsMap SceneMap;

		FBox3f MapBounds;
		float LargestZ = 0.0f;

		const float MidIndexX = StartX + static_cast<float>(CountX) / 2.0f;
		const float MidIndexY = StartY + static_cast<float>(CountY) / 2.0f;

		for (int32 CellY = StartY; CellY < (StartY + CountY); ++CellY)
		{
			for (int32 CellX = StartX; CellX < (StartX + CountX); ++CellX)
			{
				//TArray<FACUnrealScaledResourceInfo> SceneResources = GetLandblockSceneResources(PortalRegion, CellX, CellY);
				TResourceInfoArray SceneResources = GetLandblockSceneResources(PortalRegion, CellX, CellY);
				// move from center of cell, to relative to center of grid
				const float OffsetX = (CellX - MidIndexX) * 192.0f;
				const float OffsetY = (CellY - MidIndexY) * 192.0f;
				//for (FACUnrealScaledResourceInfo& ResourceInfo : SceneResources)
				for (FACUnrealResourceInfo& ResourceInfo : SceneResources)
				{
					ResourceInfo.Location.OffsetX += OffsetX;
					ResourceInfo.Location.OffsetY += OffsetY;

					if (LargestZ < FMath::Abs(ResourceInfo.Location.OffsetZ))
					{
						LargestZ = FMath::Abs(ResourceInfo.Location.OffsetZ);
					}
				}

				MapResources(SceneResources, SceneMap);

				const FBox3f CellBounds = GetLandblockBounds(PortalRegion, CellX, CellY);
				if (MapBounds.IsValid)
				{
					MapBounds += CellBounds;
				}
				else
				{
					MapBounds = CellBounds;
				}
			}
		}

		FACUnrealT3dSection LevelSection(TEXT("Level"), TEXT(""), NextDepth);
		OutContent.Append(FString(LevelSection.GetContent()));

		FACUnrealT3dSection ActorSection(TEXT("Actor"), FString::Printf(TEXT(" Class=/Script/Engine.Actor Name=Landblock_%03d_%03d_%03d_%03d Archetype=/Script/Engine.Actor'/Script/Engine.Default__Actor'"), StartX, StartY, StartX + CountX, StartY + CountY), NextDepth + 1);
		OutContent.Append(FString(ActorSection.GetContent()));

		TArray<uint32> SceneMapKeys;
		const int32 NumSceneKeys = SceneMap.GetKeys(SceneMapKeys);

		// resource headers
		FACUnrealT3dSection BoundsBoxRootSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BoxComponent Name=\"DefaultSceneRoot\""), NextDepth + 2);
		OutContent.Append(FString(BoundsBoxRootSection.GetContent()));
		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BodySetup Name=\"BodySetup_0\""), NextDepth + 3)));
		BoundsBoxRootSection.AppendFooter(OutContent);

		{
			int32 SceneObjectIndex = 0;
			for (uint32 Key : SceneMapKeys)
			{
				if (1 < SceneMap[Key]->Num())
				{
					FACUnrealT3dSection InstancedMeshSection(TEXT("Object"), FString::Printf(TEXT(" Class=/Script/Engine.InstancedStaticMeshComponent Name=\"%s%d_0x%08X_Instanced\""), *NamePrefixScene, SceneObjectIndex, Key), NextDepth + 2);
					OutContent.Append(FString(InstancedMeshSection.GetContent()));
					InstancedMeshSection.AppendBody(OutContent);
					InstancedMeshSection.AppendFooter(OutContent);
				}
				else
				{
					FACUnrealResourceInfo StaticObject(Key, (*SceneMap[Key])[0]);
					AppendResourceHeader(PortalFile, StaticObject, NamePrefixScene, SceneObjectIndex, NextDepth, OutContent);
				}

				SceneObjectIndex++;
			}
		}

		// resource bodies
		{
			int32 SceneObjectIndex = 0;
			for (uint32 Key : SceneMapKeys)
			{
				const TSharedPtr<TArray<FACUnrealResourceLocation>>& Locations = SceneMap[Key];
				if (1 < SceneMap[Key]->Num())
				{
					FACUnrealT3dLandblockStaticObjectInstanced InstancedObject(Key, SceneObjectIndex, NextDepth + 2, TEXT("BoxComponent"), TEXT("DefaultSceneRoot"), false, false, NamePrefixScene);

					for (int32 InstanceIndex = 0; InstanceIndex < Locations->Num(); ++InstanceIndex)
					{
						FACUnrealResourceLocation ObjectLocation = (*Locations)[InstanceIndex];
						ObjectLocation.OffsetX -= 96.0f;
						ObjectLocation.OffsetY -= 96.0f;
						InstancedObject.AddInstance(ObjectLocation);
					}

					OutContent.Append(FString(InstancedObject.GetContent()));
					InstancedObject.AppendPerInstanceData(OutContent);
					InstancedObject.AppendBody(OutContent);
					InstancedObject.AppendFooter(OutContent);
				}
				else
				{
					FACUnrealResourceLocation ObjectLocation = (*Locations)[0];
					ObjectLocation.OffsetX -= 96.0f;
					ObjectLocation.OffsetY -= 96.0f;
					FACUnrealResourceInfo StaticObject(Key, ObjectLocation);
					AppendResourceBody(PortalFile, StaticObject, NamePrefixScene, SceneObjectIndex, NextDepth, OutContent);;
				}

				SceneObjectIndex++;
			}
		}

		const float WidthX = static_cast<float>(CountX) * 19200.0f;
		const float WidthY = static_cast<float>(CountY) * 19200.0f;

		FACUnrealT3dSection SceneRootSection(TEXT("Object"), TEXT(" Name=\"DefaultSceneRoot\""), NextDepth + 2);
		OutContent.Append(FString(SceneRootSection.GetContent()));

		FACUnrealT3dSection SceneRootBodySetupSection(TEXT("Object"), TEXT(" Class=/Script/Engine.BodySetup Name=\"BodySetup_0\""), NextDepth + 3);
		OutContent.Append(FString(SceneRootBodySetupSection.GetContent()));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("AggGeom=(BoxElems=((TM=(XPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=-0.000000),YPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=0.000000),ZPlane=(W=0.000000,X=0.000000,Y=0.000000,Z=-0.000000),WPlane=(W=0.000000,X=-76802848.000000,Y=0.000000,Z=0.000000)),X=19200.000000,Y=19200.000000,Z=25600.000000)))\r\n"), NextDepth + 4)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CollisionTraceFlag=CTF_UseSimpleAsComplex\r\n"), NextDepth + 4)));
		SceneRootBodySetupSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("BoxExtent=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), WidthX * 0.5f, WidthY * 0.5f, LargestZ * 100.0f), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bGenerateOverlapEvents=False\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CanCharacterStepUpOn=ECB_No\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("BodyInstance=(ObjectType=ECC_WorldStatic,CollisionProfileName=\"NoCollision\",CollisionResponses=(ResponseArray=((Channel=\"Visibility\",Response=ECR_Ignore),(Channel=\"Camera\",Response=ECR_Ignore))),CollisionEnabled=NoCollision,MaxAngularVelocity=3599.999756)\r\n"), NextDepth + 3)));

		const float LandblockX = static_cast<float>(StartX - 127) * 19200.0f + WidthX * 0.5f;
		const float LandblockY = static_cast<float>(127 - StartY) * 19200.0f - WidthY * 0.5f;
		const float LandblockZ = 0.0f;
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("RelativeLocation=(X=%1.6f,Y=%1.6f,Z=%1.6f)\r\n"), LandblockX, LandblockY, LandblockZ), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("bVisualizeComponent=True\r\n"), NextDepth + 3)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("CreationMethod=Instance\r\n"), NextDepth + 3)));
		SceneRootSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dContent(TEXT("RootComponent=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("ActorLabel=\"Landblock_%03d_%03d_%03d_%03d\"\r\n"), StartX, StartY, StartX + CountX, StartY + CountY), NextDepth + 2)));
		OutContent.Append(FString(FACUnrealT3dContent(TEXT("InstanceComponents(0)=SceneComponent'\"DefaultSceneRoot\"'\r\n"), NextDepth + 2)));

		int32 ResourceIndex = 1;
		// resource footers
		{
			int32 SceneObjectIndex = 0;
			for (uint32 Key : SceneMapKeys)
			{
				if (1 < SceneMap[Key]->Num())
				{
					OutContent.Append(FString(FACUnrealT3dContent(FString::Printf(TEXT("InstanceComponents(%d)=InstancedStaticMeshComponent'\"%s%d_0x%08X_Instanced\"'\r\n"), ResourceIndex++, *NamePrefixScene, SceneObjectIndex, Key), NextDepth + 2)));
				}
				else
				{
					FACUnrealResourceInfo StaticObject(Key, (*SceneMap[Key])[0]);
					ResourceIndex = AppendResourceFooter(PortalFile, StaticObject, NamePrefixScene, SceneObjectIndex, ResourceIndex, NextDepth, OutContent);
				}

				SceneObjectIndex++;
			}
		}

		ActorSection.AppendFooter(OutContent);
		LevelSection.AppendFooter(OutContent);

		OutContent.Append(FString(FACUnrealT3dSection(TEXT("Surface"), TEXT(""), TabDepth)));
	}

	int32 StartX;
	int32 StartY;
	int32 CountX;
	int32 CountY;
};

FString FACUnrealT3dWriter::ExportText(const int32 CellX, const int32 CellY, const FACUnrealCellLandblockObject& LandblockInfos)
{
	FString OutContent;
	OutContent.Append(FString(FACUnrealT3dMap(CellX, CellY, LandblockInfos)));

	return OutContent;
}

FString FACUnrealT3dWriter::ExportLandblockScenesText(const int32 StartX, const int32 StartY, const int32 CountX, const int32 CountY)
{
	FString OutContent;
	OutContent.Append(FString(FACUnrealT3dMapScene(StartX, StartY, CountX, CountY)));

	return OutContent;
}

FString FACUnrealT3dWriter::ExportText(const FACUnrealPortalScene& Scene)
{
	FString OutContent;
	OutContent.Append(FString(FACUnrealT3dScene(Scene)));

	return OutContent;
}

FString FACUnrealT3dWriter::ExportText(const FACUnrealPortalSetup& Setup)
{
	FString OutContent;
	OutContent.Append(FString(FACUnrealT3dSetup(Setup)));

	return OutContent;
}
