#pragma once

class FACUnrealByteReader;

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionHourBlock
{
	FACUnrealPortalRegionHourBlock();

	float StartTime;

	uint32 IsNightTime;

	FString HourName;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionSeason
{
	FACUnrealPortalRegionSeason();

	uint32 StartDay;

	FString SeasonName;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionWeatherColorUnknown
{
	FACUnrealPortalRegionWeatherColorUnknown();

	uint32 Unk1;

	float Unk2[5];
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionWeatherColor
{
	FACUnrealPortalRegionWeatherColor();

	float Unk1[4];

	uint32 ColorA;

	float Unk2;

	uint32 ColorB;

	float Unk3[2];

	uint32 Color3;

	uint32 Unk4;

	uint32 NumWeatherColorUnknowns;

	TArray<FACUnrealPortalRegionWeatherColorUnknown> WeatherColorUnknowns;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionWeatherResource
{
	FACUnrealPortalRegionWeatherResource();

	float Unk1[6];

	uint32 ResourceId;

	uint32 ResourceId2;

	uint32 Unk2;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionWeather
{
	FACUnrealPortalRegionWeather();

	float Percentage;

	FString Name;

	uint32 NumWeatherResources;

	TArray<FACUnrealPortalRegionWeatherResource> WeatherResources;

	uint32 NumWeatherColors;

	TArray<FACUnrealPortalRegionWeatherColor> WeatherColors;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionUnknownB
{
	FACUnrealPortalRegionUnknownB();

	uint32 Unk1;

	float Unk2[5];
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionUnknownA
{
	FACUnrealPortalRegionUnknownA();

	uint32 Unk1;

	uint32 NumUnknownBs;

	TArray<FACUnrealPortalRegionUnknownB> UnknownBs;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionSceneType
{
	FACUnrealPortalRegionSceneType();

	uint32 SceneTypeUnk;

	uint32 SceneCount;

	TArray<uint32> SceneIds;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionTerrainType
{
	FACUnrealPortalRegionTerrainType();

	FString TerrainName;

	uint32 TerrainColor;

	uint32 NumTerrainSceneTypes;

	TArray<uint32> TerrainSceneTypes;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionAlphaTexture
{
	FACUnrealPortalRegionAlphaTexture();

	uint32 TexCode;

	uint32 TexId;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionAlphaTextures
{
	FACUnrealPortalRegionAlphaTextures();

	uint32 NumAlphaTex;

	TArray<FACUnrealPortalRegionAlphaTexture> AlphaTextures;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegionTerrainTexture
{
	FACUnrealPortalRegionTerrainTexture();

	uint32 TerrainTexNum;

	uint32 TerrainTexResourceId;

	uint32 TexTiling;

	uint32 MaxVertBright;

	uint32 MinVertBright;

	uint32 MaxVertSaturate;

	uint32 MinVertSaturate;

	uint32 MaxVertHue;

	uint32 MinVertHue;

	uint32 DetailTexTiling;

	uint32 DetailTexId;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalRegion
{
	FACUnrealPortalRegion();
	FACUnrealPortalRegion(FACUnrealByteReader& Reader);

	uint32 ResourceId;

	uint32 RegionNumber;

	uint32 RegionVersion;

	FString RegionName;

	uint32 NumRows;

	uint32 NumColumns;

	float SquareLength;
	uint32 heightsPerCell;
	uint32 VertexPerCell;
	float Unk1;
	float Unk2;
	float RoadWidth;

	TArray<float> HeightValues;

	uint32 Unk4;
	float Unk5;
	uint32 Unk6;
	float Unk7;
	uint32 daysPerYear;
	FString yearUnitName;

	uint32 NumHours;

	TArray<FACUnrealPortalRegionHourBlock> HourBlocks;

	uint32 NumHolidays;

	TArray<FString> HolidayNames;

	uint32 NumSeasons;

	TArray<FACUnrealPortalRegionSeason> Seasons;

	uint32 unknown8;
	uint32 unknown9;
	float unknown10;
	float unknown11;
	float unknown12;

	uint32 NumWeather;

	TArray<FACUnrealPortalRegionWeather> Weathers;

	uint32 NumUnknownA;

	TArray<FACUnrealPortalRegionUnknownA> UnknownAs;

	uint32 NumSceneTypes;

	TArray<FACUnrealPortalRegionSceneType> SceneTypes;

	uint32 NumTerrainTypes;

	TArray<FACUnrealPortalRegionTerrainType> TerrainTypes;

	uint32 Unk9;

	uint32 Unk10;

	FACUnrealPortalRegionAlphaTextures AlphaTexture1;

	FACUnrealPortalRegionAlphaTextures AlphaTexture2;

	FACUnrealPortalRegionAlphaTextures AlphaTexture3;

	uint32 NumTerrainTex;

	TArray<FACUnrealPortalRegionTerrainTexture> TerrainTextures;

	uint32 Unk11;

	uint32 SmallMap;

	uint32 LargeMap;

	const uint8* Unk12;
};
