#pragma once

struct FACUnrealPortalModel;
struct FACUnrealPortalEnvironment;

struct ACUNREALDATTOOLS_API FACUnrealFbxContent
{
	FACUnrealFbxContent(int32 InDepth);
	FACUnrealFbxContent(const FString& InContent, int32 InDepth);
	virtual ~FACUnrealFbxContent() {}

	virtual operator FString() const { return Content; }

	const FString& GetContent() const { return Content; }

protected:
	FString Content;
	FString Indent;
	const int32 Depth;
	const int32 PrevDepth;
	const int32 NextDepth;
};

struct ACUNREALDATTOOLS_API FACUnrealFbxValue : public FACUnrealFbxContent
{
	FACUnrealFbxValue(const FString& InKey, const FString& InValue, int32 InDepth);
	FACUnrealFbxValue(const FString& InKey, const int32& InValue, int32 InDepth);
	FACUnrealFbxValue(const FString& InKey, const float& InValue, int32 InDepth);
	FACUnrealFbxValue(const FString& InKey, const TArray<FString>& InValue, int32 InDepth);
	FACUnrealFbxValue(const FString& InKey, const TArray<int32>& InValue, int32 InDepth);
	FACUnrealFbxValue(const FString& InKey, const TArray<float>& InValue, int32 InDepth);
	virtual ~FACUnrealFbxValue() {}
};

struct ACUNREALDATTOOLS_API FACUnrealFbxSection : public FACUnrealFbxContent
{
	FACUnrealFbxSection(const FString& InSectionTag, int32 InDepth);
	FACUnrealFbxSection(const FString& InSectionTag, const FString& InSectionName, int32 InDepth);
	FACUnrealFbxSection(const FString& InSectionTag, const int32& InSectionId, int32 InDepth);

	virtual ~FACUnrealFbxSection() {}

	virtual operator FString() const override;

	virtual void AppendBody(FString& OutContent) const {}

	void AppendFooter(FString& OutContent) const;
};

struct ACUNREALDATTOOLS_API FACUnrealFbxWriter
{
	static FString ExportText(const FACUnrealPortalEnvironment& Environment);

	static FString ExportPortalsText(const FACUnrealPortalEnvironment& Environment);

	static FString ExportText(const FACUnrealPortalModel& Model);
};
