#pragma once

class FACUnrealByteReader;

struct ACUNREALDATTOOLS_API FACUnrealPortalSceneObject
{
	//
	FACUnrealResourceInfo ResourceInfo;
	//
	float Frequency;
	//
	float DisplaceX;
	//
	float DisplaceY;
	//
	float MinScale;
	//
	float MaxScale;
	//
	float MaxRotation;
	//
	float MinSlope;
	//
	float MaxSlope;
	//
	uint32 IntAlign;
	//
	uint32 IntOrient;
	//
	uint32 IntIsWeenieObj;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalScene
{
	FACUnrealPortalScene();
	FACUnrealPortalScene(FACUnrealByteReader& Reader);

	//
	uint32 ResourceId;
	//
	uint32 NumObects;
	//
	TArray<FACUnrealPortalSceneObject> Objects;
};
