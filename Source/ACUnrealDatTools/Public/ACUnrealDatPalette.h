#pragma once

class FACUnrealByteReader;

struct ACUNREALDATTOOLS_API FACUnrealPortalPalette
{
	FACUnrealPortalPalette();
	FACUnrealPortalPalette(FACUnrealByteReader& Reader);
	//
	uint32 ResourceId;
	//
	uint32 NumColors;
	//
	TArray<FColor> Colors;
};
