#pragma once

class FACUnrealByteReader;

struct ACUNREALDATTOOLS_API FACUnrealSetupLight
{
	FACUnrealSetupLight();
	//
	int32 LightIndex;
	//
	FACUnrealResourceLocation Location;
	//
	int32 Color;
	//
	float Intensity;
	//
	float Falloff;
	//
	float ConeAngle;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalSetup
{
	FACUnrealPortalSetup();
	FACUnrealPortalSetup(FACUnrealByteReader& Reader);
	//
	uint32 ResourceId;
	//
	uint32 Flags;
	//
	TArray<int32> ModelResourceIds;
	//
	TArray<int32> ModelParents;
	//
	TArray<FVector3f> ModelScales;
	//
	TMap<int32, FACUnrealResourceInfo> HoldingLocations;
	//
	TMap<int32, FACUnrealResourceInfo> ConnectionPoints;
	//
	TArray<int32> OrderedPlacementKeys;
	//
	TMap<int32, TArray<FACUnrealResourceLocation>> PlacementFrames;
	//
	int32 LightCount;
	//
	TArray<FACUnrealSetupLight> Lights;
	//
	int32 DefaultAnimId;
	//
	int32 DefaultPhysScriptId;
	//
	int32 DefaultMotionTableId;
	//
	int32 DefaultSoundTableId;
	//
	int32 DefaultPhysScriptTableId;
};
