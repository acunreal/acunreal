#pragma once

class FACUnrealByteReader;

struct ACUNREALDATTOOLS_API FACUnrealPortalSurface
{
	FACUnrealPortalSurface();
	FACUnrealPortalSurface(FACUnrealByteReader& Reader);
	//
	uint32 Flags;
	//
	uint32 BGRA;
	//
	uint32 TextureID;
	//
	uint32 PaletteID;
	//
	float Translucency;
	//
	float Luminosity;
	//
	float Diffuse;
};
