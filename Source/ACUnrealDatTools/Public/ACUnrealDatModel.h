#pragma once

class FACUnrealByteReader;

namespace EACUnrealPortalModelBSPTree
{
	enum Type : int32
	{
		kDrawing = 0,
		kPhysics = 1,
		kCell = 2
	};
}

namespace EACUnrealPortalModelBSPNode
{
	enum Type : uint32
	{
		kNone = 0x0,
		kLeaf = 0x4c454146,
		kPortal = 0x504f5254,
		kBpnn = 0x42506e6e,
		kBPIn = 0x4250496e,
		kBpIN = 0x4270494e,
		kBpnN = 0x42706e4e,
		kBPIN = 0x4250494e,
		kBPnN = 0x42506e4e,
		kUnkn1 = 0x42504f4c,
		kUnkn2 = 0x4270496e,
		kUnkn3 = 0x4250464c,
	};
}

namespace EACUnrealPortalModelFlags
{
	enum Type : uint32
	{
		kNone = 0x0,
		kHasPhysicsBSP = 0x1,
		kHasDrawingBSP = 0x2,
		kHasDegrade = 0x8
	};
}

namespace EACUnrealPortalModelStippling
{
	enum Type : uint8
	{
		kNoStippling = 0,
		kPositiveStippling = 1,
		kNegativeStippling = 2,
		kNoPosUVs = 4,
		kNoNegUVs = 8,
	};
}

namespace EACUnrealPortalModelSides
{
	enum Type : int32
	{
		kSingle = 0,
		kDouble = 1,
		kBoth = 2
	};
}

struct ACUNREALDATTOOLS_API FACUnrealPortalModelVertex
{
	FACUnrealPortalModelVertex();
	FACUnrealPortalModelVertex(const FACUnrealPortalModelVertex& Other);
	FACUnrealPortalModelVertex(FACUnrealByteReader& Reader);
	FACUnrealPortalModelVertex& operator=(const FACUnrealPortalModelVertex& Other);
	//
	uint16 Index;
	//
	uint16 NumTexCoords;
	//
	FVector3f Position;
	//
	FVector3f Normal;
	//
	TArray<FVector2f> UVs;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalModelTriangleFan
{
	FACUnrealPortalModelTriangleFan();
	FACUnrealPortalModelTriangleFan(const FACUnrealPortalModelTriangleFan& Other);
	FACUnrealPortalModelTriangleFan(FACUnrealByteReader& Reader);
	FACUnrealPortalModelTriangleFan& operator=(const FACUnrealPortalModelTriangleFan& Other);
	bool operator<(const FACUnrealPortalModelTriangleFan& Other) const;

	uint16 FanIndex;
	uint8 NumIndices;
	EACUnrealPortalModelStippling::Type Stippling;
	EACUnrealPortalModelSides::Type Sides;
	uint16 SurfaceIndex;
	uint16 SurfaceUnk;
	TArray<uint16> VertexIndices;
	TArray<uint8> TexCoordIndices;
};

typedef TSharedPtr<struct FACUnrealPortalModelBSPNode> TACUnrealBSPNodePtr;

struct ACUNREALDATTOOLS_API FACUnrealPortalModelBSPNode
{
	FACUnrealPortalModelBSPNode(EACUnrealPortalModelBSPTree::Type TreeType, EACUnrealPortalModelBSPNode::Type PlaneType);
	FACUnrealPortalModelBSPNode(const FACUnrealPortalModelBSPNode& Other);
	FACUnrealPortalModelBSPNode(EACUnrealPortalModelBSPTree::Type TreeType, EACUnrealPortalModelBSPNode::Type PlaneType, FACUnrealByteReader& Reader);
	FACUnrealPortalModelBSPNode& operator=(const FACUnrealPortalModelBSPNode& Other);

	EACUnrealPortalModelBSPTree::Type Tree;
	EACUnrealPortalModelBSPNode::Type Node;
	FSphere3f Bounds;
	FPlane4f Partition;
	mutable TACUnrealBSPNodePtr FrontChild;
	mutable TACUnrealBSPNodePtr BackChild;
	TArray<uint16> TriangleIndices;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalModelBSPLeaf : public FACUnrealPortalModelBSPNode
{
	FACUnrealPortalModelBSPLeaf(EACUnrealPortalModelBSPTree::Type TreeType);
	FACUnrealPortalModelBSPLeaf(const FACUnrealPortalModelBSPLeaf& Other);
	FACUnrealPortalModelBSPLeaf(EACUnrealPortalModelBSPTree::Type TreeType, FACUnrealByteReader& Reader);
	FACUnrealPortalModelBSPLeaf& operator=(const FACUnrealPortalModelBSPLeaf& Other);

	uint32 Index;
	uint32 Solid;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalModelBSPPortalPoly
{
	FACUnrealPortalModelBSPPortalPoly(uint16 InPortalIndex, uint16 InPolygonIndex);
	FACUnrealPortalModelBSPPortalPoly(const FACUnrealPortalModelBSPPortalPoly& Other);
	FACUnrealPortalModelBSPPortalPoly(FACUnrealByteReader& Reader);
	FACUnrealPortalModelBSPPortalPoly& operator=(const FACUnrealPortalModelBSPPortalPoly& Other);

	uint16 PortalIndex;
	uint16 PolygonIndex;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalModelBSPPortal : public FACUnrealPortalModelBSPNode
{
	FACUnrealPortalModelBSPPortal(EACUnrealPortalModelBSPTree::Type TreeType);
	FACUnrealPortalModelBSPPortal(const FACUnrealPortalModelBSPPortal& Other);
	FACUnrealPortalModelBSPPortal(EACUnrealPortalModelBSPTree::Type TreeType, FACUnrealByteReader& Reader);
	FACUnrealPortalModelBSPPortal& operator=(const FACUnrealPortalModelBSPPortal& Other);

	TArray<FACUnrealPortalModelBSPPortalPoly> PortalPolys;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalModel
{
	FACUnrealPortalModel();
	FACUnrealPortalModel(const FACUnrealPortalModel& Other);
	FACUnrealPortalModel(FACUnrealByteReader& Reader);
	FACUnrealPortalModel& operator=(const FACUnrealPortalModel& Other);
	//
	uint32 ResourceId;
	//
	EACUnrealPortalModelFlags::Type Flags;
	//
	uint8 SurfaceCount;
	//
	TArray<uint32> SurfaceIDs;
	//
	TArray<FACUnrealPortalModelVertex> Vertices;
	//
	uint16 NumCollisionFans;
	//
	TArray<FACUnrealPortalModelTriangleFan> CollisionFans;
	//
	mutable TACUnrealBSPNodePtr CollisionBSP;
	//
	uint16 NumRenderFans;
	//
	TArray<FACUnrealPortalModelTriangleFan> RenderFans;
	//
	mutable TACUnrealBSPNodePtr RenderBSP;
	//
	TArray<TACUnrealBSPNodePtr> PortalNodes;
};

extern void ReadBSP(FACUnrealByteReader& Reader, TACUnrealBSPNodePtr& Node, EACUnrealPortalModelBSPTree::Type TreeType);
extern TSet<TACUnrealBSPNodePtr> WalkBSP(TACUnrealBSPNodePtr Node, EACUnrealPortalModelBSPNode::Type NodeType);
