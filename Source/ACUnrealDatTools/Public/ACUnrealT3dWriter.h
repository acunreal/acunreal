#pragma once

struct FACUnrealPortalModel;

struct ACUNREALDATTOOLS_API FACUnrealT3dContent
{
	FACUnrealT3dContent(int32 InDepth);
	FACUnrealT3dContent(const FString& InContent, int32 InDepth);
	virtual ~FACUnrealT3dContent() {}

	virtual operator FString() const { return Content; }

	const FString& GetContent() const { return Content; }

protected:
	FString Content;
	FString Indent;
	const int32 TabDepth;
	const int32 PrevDepth;
	const int32 NextDepth;
};

struct ACUNREALDATTOOLS_API FACUnrealT3dSection : public FACUnrealT3dContent
{
	FACUnrealT3dSection(const FString& InSectionTag, const FString& InSectionValue, int32 InDepth);
	virtual ~FACUnrealT3dSection() {}

	virtual operator FString() const override;

	virtual void AppendBody(FString& OutContent) const {}

	void AppendFooter(FString& OutContent) const;

	FString SectionTag;
};

struct ACUNREALDATTOOLS_API FACUnrealT3dWriter
{
	static FString ExportText(const int32 CellX, const int32 CellY, const struct FACUnrealCellLandblockObject& LandblockInfos);

	static FString ExportLandblockScenesText(const int32 StartX, const int32 StartY, const int32 CountX, const int32 CountY);

	static FString ExportText(const struct FACUnrealPortalScene& Scene);

	static FString ExportText(const struct FACUnrealPortalSetup& Setup);
};
