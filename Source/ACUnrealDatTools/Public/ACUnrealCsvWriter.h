#pragma once

struct FACUnrealPortalRegion;

struct ACUNREALDATTOOLS_API FACUnrealCsvWriter
{
	static FString ExportText(const FACUnrealPortalRegion& Region);
};
