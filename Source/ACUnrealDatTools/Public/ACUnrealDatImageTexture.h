#pragma once

class FACUnrealByteReader;

struct ACUNREALDATTOOLS_API FACUnrealPortalImageTexture
{
	FACUnrealPortalImageTexture();
	FACUnrealPortalImageTexture(FACUnrealByteReader& Reader);
	//
	uint32 ResourceId;
	//
	uint32 AlwaysZero;
	//
	uint8 AlwaysTwo;
	//
	uint32 NumImgColors;
	//
	TArray<uint32> ImageColorIDs;
};
