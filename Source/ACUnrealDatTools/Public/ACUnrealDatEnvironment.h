#pragma once

#include "ACUnrealDatModel.h"

class FACUnrealByteReader;

namespace EACUnrealPortalEnvironmentPartSurface {
	enum Type
	{
		kTriangles,
		kHitTriangles,
		kPortals
	};
}

struct ACUNREALDATTOOLS_API FACUnrealPortalEnvironmentPart
{
	FACUnrealPortalEnvironmentPart();
	FACUnrealPortalEnvironmentPart(FACUnrealByteReader& Reader);

	TArray<FACUnrealPortalModelVertex> Vertices;
	TArray<FACUnrealPortalModelTriangleFan> TriangleFans;
	TArray<FACUnrealPortalModelTriangleFan> HitTriangleFans;
	TArray<uint16> Portals;
	mutable TACUnrealBSPNodePtr HitTree;

	TArray<uint16> GetSurfaceIndices(EACUnrealPortalEnvironmentPartSurface::Type SurfaceType = EACUnrealPortalEnvironmentPartSurface::kTriangles) const;
};

struct ACUNREALDATTOOLS_API FACUnrealPortalEnvironment
{
	FACUnrealPortalEnvironment();
	FACUnrealPortalEnvironment(FACUnrealByteReader& Reader);

	//
	uint32 ResourceId;
	//
	uint32 NumParts;
	//
	TArray<FACUnrealPortalEnvironmentPart> Parts;

	TArray<uint16> GetPartSurfaceIndices(bool bUnique) const
	{
		TArray<uint16> PartSurfaceIndices;

		for (const FACUnrealPortalEnvironmentPart& Part : Parts)
		{
			PartSurfaceIndices.Append(Part.GetSurfaceIndices());
		}

		if (bUnique)
		{
			TSet<uint16> Uniques;
			Uniques.Append(PartSurfaceIndices);
			PartSurfaceIndices = Uniques.Array();
		}

		PartSurfaceIndices.Sort();

		return PartSurfaceIndices;
	}
};
