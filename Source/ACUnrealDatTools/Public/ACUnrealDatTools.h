#pragma once

#include "CoreMinimal.h"
#include "Commandlets/Commandlet.h"
#include "Engine/Texture.h"
#include "Modules/ModuleInterface.h"
#include "ACUnrealDatTools.generated.h"

ACUNREALDATTOOLS_API DECLARE_LOG_CATEGORY_EXTERN(LogACUnrealDatTools, All, All);

#define ACUNREALDATTOOLS_LOG_PREFIX TEXT("ACUnrealDatTools: ")
#define UE_LOG_ACUNREALDATTOOLS(Verbosity, Format, ...) \
{ \
	UE_LOG(LogACUnrealDatTools, Verbosity, TEXT("%s%s"), ACUNREALDATTOOLS_LOG_PREFIX, *FString::Printf(Format, ##__VA_ARGS__)); \
}

const uint32 kSceneLandblockWidth = 16;

UENUM(BlueprintType)
namespace EACUnrealDatFile
{
	enum Type
	{
		Cell,
		Highres,
		LocaleEnglish,
		Portal,
	};
}

UENUM(BlueprintType)
namespace EACUnrealResource
{
	enum Type
	{
		Model,
		Setup,
		Animation,
		Palette,
		ImageTexture,
		ImageColor,
		Surface,
		MotionTable,
		Sound,
		Environment,
		Scene,
		Region,
		SoundTable,
		EnumMapper,
		ParticleEmitter,
		PhysicsScript,
		PhysicsScriptTable,
		Invalid
	};
}

struct ACUNREALDATTOOLS_API FACUnrealResourceLocation
{
	FACUnrealResourceLocation();

	//
	float OffsetX;
	//
	float OffsetY;
	//
	float OffsetZ;
	//
	float HeadingW;
	//
	float HeadingA;
	//
	float HeadingB;
	//
	float HeadingC;
	//
};

typedef TArray<FACUnrealResourceLocation> TResourceLocationArray;

struct ACUNREALDATTOOLS_API FACUnrealResourceInfo
{
	FACUnrealResourceInfo();

	FACUnrealResourceInfo(uint32 InResourceId, const FACUnrealResourceLocation& InLocation);

	//
	uint32 ResourceId;
	//
	FACUnrealResourceLocation Location;
};

typedef TArray<FACUnrealResourceInfo> TResourceInfoArray;
typedef TMap<uint32, TSharedPtr<TResourceLocationArray>> TResourceLocationsMap;

typedef TArray<uint8> TByteArray;
typedef TSharedPtr<TByteArray, ESPMode::ThreadSafe> TByteArrayPtr;
typedef TSharedPtr<class FACUnrealDatFile, ESPMode::ThreadSafe> FACUnrealDatFilePtr;

class ACUNREALDATTOOLS_API IACUnrealDatToolsModule : public IModuleInterface
{
public:
	virtual void GetDatFileDirectory(FString& OutPath, bool& OutHasCell, bool& OutHasHighRes, bool& OutHasLocale, bool& OutHasPortal) = 0;

	virtual void SetDatFileDirectory(const FString& Path) = 0;

	virtual void DumpDatFileStatsToLog(EACUnrealDatFile::Type DatFileType, bool bDumpFileEntries) = 0;

	virtual FACUnrealDatFilePtr GetDatFile(EACUnrealDatFile::Type DatFileType) = 0;

	virtual void ExportLandblocks() = 0;

	virtual void ExportLandblockInfo(uint32 LandblockX, uint32 LandblockY) = 0;

	virtual bool ExportResource(EACUnrealResource::Type ResourceType, uint32 Identifier) = 0;

	virtual int32 ExportResources(EACUnrealResource::Type ResourceType, uint32 IdentifierMask) = 0;

	virtual bool CorrectPortalImageTextureSettings(UTexture2D* Texture, uint32 Identifier = 0) = 0;

	// todo: apis for retrieving name-standards from ini settings
	// i.e. intermediate save locations, file patterns, scaling, etc.
};

UCLASS()
class UACUnrealExportResourcesCommandlet : public UCommandlet
{
	GENERATED_BODY()

	/** Parsed commandline tokens */
	TArray<FString> Tokens;

	/** Parsed commandline switches */
	TArray<FString> Switches;

public:
	UACUnrealExportResourcesCommandlet();

public:
	virtual int32 Main(const FString& Params) override;
};

bool SaveImageToFile(const TArray<FColor>& ImageColors, int32 Width, int32 Height, const FString& FileName, const TCHAR* SavedSubDir = TEXT("/Images/"));

template<typename INT_TYPE>
void ArrayToString(const TArray<INT_TYPE>& Array, FString& String)
{
	const int32 Count = Array.Num();
	for (int32 i = 0; i < Count; ++i)
	{
		if (i > 0)
		{
			String += TEXT(",");
		}
		String += FString::Printf(TEXT("%d"), Array[i]);
	}
}

template<typename INT_TYPE>
void ArrayToHexString(const TArray<INT_TYPE>& Array, FString& String)
{
	const int32 Count = Array.Num();
	for (int32 i = 0; i < Count; ++i)
	{
		if (i > 0)
		{
			String += TEXT(",");
		}
		String += FString::Printf(TEXT("0x%08X"), Array[i]);
	}
}

void ArrayToString(const TArray<FString>& Array, FString& String);
void ArrayToString(const TArray<float>& Array, FString& String);

FACUnrealResourceLocation SumResourceLocation(const FACUnrealResourceLocation& Left, const FACUnrealResourceLocation& Right);
