#pragma once

#include "ACUnrealFbxWriter.h"
#include "ACUnrealDatModel.h"

struct ACUNREALDATTOOLS_API FACUnrealFbxModel : public FACUnrealFbxSection
{
	FACUnrealFbxModel(const TArray<FACUnrealPortalModelTriangleFan>& InFans, const TArray<FACUnrealPortalModelVertex>& InVertices, int32 InSurfaceIndex, bool bInFilterSurfaceFans = true, bool bInCollisionOnly = false, bool bInCollisionHasTexture = false, const FString& SurfaceName = TEXT("Surface"));

	virtual void AppendBody(FString& OutContent) const override;

	TArray<FACUnrealPortalModelTriangleFan> Fans;
	TArray<FACUnrealPortalModelVertex> Vertices;
	int32 SurfaceIndex;
	bool bFilterSurfaceFans;
	bool bCollisionOnly;
	bool bCollisionHasTexture;
};
