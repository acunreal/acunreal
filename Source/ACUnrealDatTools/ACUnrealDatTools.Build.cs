using UnrealBuildTool;

public class ACUnrealDatTools : ModuleRules
{
	public ACUnrealDatTools(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivatePCHHeaderFile = "Public/ACUnrealDatTools.h";

		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"CoreUObject",
				"Engine",
				"ImageWrapper",
			}
		);
	}
}
