using UnrealBuildTool;
using System.Collections.Generic;

public class ACUnrealEditorTarget : TargetRules
{
	public ACUnrealEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("ACUnreal");
		ExtraModuleNames.Add("ACUnrealEditor");
	}
}
