#include "ACUnrealResourceFactoryComponent.h"
#include "ACUnreal/ACUnreal.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include "Engine/AssetManager.h"

UACUnrealResourceFactoryComponent::UACUnrealResourceFactoryComponent()
{
}

void UACUnrealResourceFactoryComponent::OnRegister()
{
	Super::OnRegister();

	if (IsReadyToLoadResources())
	{
		LoadResources();
	}
}

bool UACUnrealResourceFactoryComponent::IsReadyToLoadResources() const
{
#if WITH_EDITORONLY_DATA
	if (GIsEditor && GetWorld() && !GetWorld()->IsGameWorld())
		return true;
#endif

	return GetWorld() && GetWorld()->IsGameWorld();
}

int32 UACUnrealResourceFactoryComponent::FindResource(const FString& Name) const
{
	for (int32 Index = 0; Index < Resources.Num(); ++Index)
	{
		if (Name.Compare(ToComponentName(Resources[Index], Index), ESearchCase::IgnoreCase) == 0)
		{
			return Index;
		}
	}

	return INDEX_NONE;
}

UActorComponent* UACUnrealResourceFactoryComponent::FindResourceComponent(const FACUnrealResourceFactoryData& Data, int32 ResourceIndex)
{
	const FString ComponentName = ToComponentName(Data, ResourceIndex);

	TArray<UActorComponent*>& ComponentsToSearch = ResourceComponents;

	UActorComponent** FoundComponent = ResourceComponents.FindByPredicate([this, ComponentName](UActorComponent* Component)
		{
			const bool bNameMatches = Component && Component->GetName().Compare(ComponentName, ESearchCase::IgnoreCase) == 0;
#if WITH_EDITORONLY_DATA
			return bNameMatches && GetWorld() && (GetWorld()->IsGameWorld() == !Component->IsEditorOnly());
#else
			return bNameMatches;
#endif
		});

	return FoundComponent ? *FoundComponent : nullptr;
}

UActorComponent* UACUnrealResourceFactoryComponent::CreateResourceComponent(const FACUnrealResourceFactoryData& Data, int32 ResourceIndex)
{
	const FString ComponentName = ToComponentName(Data, ResourceIndex);
	UActorComponent* Component = NewObject<UStaticMeshComponent>(this, FName(ComponentName));

#if WITH_EDITORONLY_DATA
	if (GIsEditor && GetWorld() && !GetWorld()->IsGameWorld())
	{
		Component->SetIsVisualizationComponent(true);
	}
#endif

	if (Component)
	{
		Component->RegisterComponent();
		GetOwner()->AddInstanceComponent(Component);
		ResourceComponents.Add(Component);
	}

	return Component;
}

void UACUnrealResourceFactoryComponent::LoadResources()
{
#if WITH_EDITORONLY_DATA
	PruneDanglingComponents();
#else
	check(0 == PruneDanglingComponents());
#endif

	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");

	for (const FACUnrealResourceFactoryData& Resource : Resources)
	{
		const FSoftObjectPath SoftPath = ToSoftObjectPath(Resource);

		FStreamableDelegate AssetLoadedDelegate = FStreamableDelegate::CreateWeakLambda(this, [this, SoftPath]() mutable
		{
			if (UObject* LoadedObject = SoftPath.ResolveObject())
			{
				LoadComplete(LoadedObject);
			}
			else
			{
				UE_LOG(LogACUnreal, Warning, TEXT("%s::LoadResources() - failed to load UObject for resource %s"), *SoftPath.ToString());
			}
		});

		if (UObject* LoadedObject = SoftPath.ResolveObject())
		{
			AssetLoadedDelegate.Execute();
		}
#if WITH_EDITORONLY_DATA
		else if (GIsEditor && GetWorld() && !GetWorld()->IsGameWorld())
		{
			auto Handle = UAssetManager::GetStreamableManager().RequestSyncLoad(SoftPath);
			if (UObject* SyncLoadedObject = Handle->GetLoadedAsset())
			{
				LoadComplete(SyncLoadedObject);
			}
		}
#endif
		else
		{
			UAssetManager::GetStreamableManager().RequestAsyncLoad(SoftPath, AssetLoadedDelegate);
		}
	}
}

void UACUnrealResourceFactoryComponent::LoadComplete(UObject* Loaded)
{
	if (auto AsStaticMesh = Cast<UStaticMesh>(Loaded))
	{
		for (int32 Index = 0; Index < Resources.Num(); ++Index)
		{
			const FACUnrealResourceFactoryData& Resource = Resources[Index];

			auto SoftPath = ToSoftObjectPath(Resource);
			if (SoftPath.ResolveObject() == Loaded)
			{
				UActorComponent* Component = FindResourceComponent(Resource, Index);
				if (!Component)
					Component = CreateResourceComponent(Resource, Index);

				if (Component)
				{
					if (UStaticMeshComponent* MeshComponent = Cast<UStaticMeshComponent>(Component))
					{
						MeshComponent->SetStaticMesh(AsStaticMesh);
					}

					if (USceneComponent* SceneComponent = Cast<USceneComponent>(Component))
					{
						SceneComponent->AttachToComponent(GetOwner()->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
						SceneComponent->SetRelativeTransform(Resource.RelativeTransform);
					}
				}
			}
		}
	}
}

FSoftObjectPath UACUnrealResourceFactoryComponent::ToSoftObjectPath(const FACUnrealResourceFactoryData& Data)
{
	return FSoftObjectPath(FString::Printf(TEXT("/Game/Models/0x%08X.0x%08X"), Data.ResourceId, Data.ResourceId));
}

FString UACUnrealResourceFactoryComponent::ToComponentName(const FACUnrealResourceFactoryData& Data, int32 Index)
{
	return FString::Printf(TEXT("[%d]0x%08X"), Index, Data.ResourceId);
}

int32 UACUnrealResourceFactoryComponent::PruneDanglingComponents()
{
	TArray<UActorComponent*> DanglingComponents;
	for (UActorComponent* ResourceComponent : ResourceComponents)
	{
		if (INDEX_NONE == FindResource(ResourceComponent->GetName()))
		{
			DanglingComponents.Add(ResourceComponent);
		}
	}

	for (UActorComponent* DoomedComponent : DanglingComponents)
	{
		ResourceComponents.Remove(DoomedComponent);
		GetOwner()->RemoveInstanceComponent(DoomedComponent);
		DoomedComponent->DestroyComponent();
	}

	return DanglingComponents.Num();
}
