#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ACUnrealGameMode.generated.h"

UCLASS(minimalapi)
class AACUnrealGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AACUnrealGameMode();
};

