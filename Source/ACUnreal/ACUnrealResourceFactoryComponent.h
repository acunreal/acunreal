#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ACUnrealResourceFactoryComponent.generated.h"

USTRUCT(BlueprintType)
struct ACUNREAL_API FACUnrealResourceFactoryData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	int32 ResourceId;

	UPROPERTY(VisibleAnywhere)
	FSoftObjectPath ResourcePath;

	UPROPERTY(EditAnywhere)
	FTransform RelativeTransform;
};

UCLASS(BlueprintType, Blueprintable, meta = (BlueprintSpawnableComponent))
class ACUNREAL_API UACUnrealResourceFactoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UACUnrealResourceFactoryComponent();

protected:
	virtual void OnRegister() override;

	bool IsReadyToLoadResources() const;

	int32 FindResource(const FString& Name) const;

	UActorComponent* FindResourceComponent(const FACUnrealResourceFactoryData& Data, int32 ResourceIndex);

	UActorComponent* CreateResourceComponent(const FACUnrealResourceFactoryData& Data, int32 ResourceIndex);

	void LoadResources();

	UFUNCTION()
	void LoadComplete(UObject* Loaded);

	static FSoftObjectPath ToSoftObjectPath(const FACUnrealResourceFactoryData& Data);

	static FString ToComponentName(const FACUnrealResourceFactoryData& Data, int32 Index);

	int32 PruneDanglingComponents();

protected:
	UPROPERTY(EditDefaultsOnly)
	TArray<FACUnrealResourceFactoryData> Resources;

	UPROPERTY(Transient)
	TArray<UActorComponent*> ResourceComponents;
};
