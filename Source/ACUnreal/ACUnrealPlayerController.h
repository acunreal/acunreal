#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ACUnrealPlayerController.generated.h"

UCLASS(Blueprintable)
class AACUnrealPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AACUnrealPlayerController();

protected:
	virtual void AttachToPawn(APawn* InPawn) override;

	UFUNCTION(BlueprintImplementableEvent, Category=Game, meta=(DisplayName="OnAttachToPawn", ScriptName="OnAttachToPawn"))
	void K2_OnAttachToPawn(APawn* InPawn);
};
