#include "ACUnrealGameMode.h"
#include "ACUnrealCharacter.h"
#include "UObject/ConstructorHelpers.h"

AACUnrealGameMode::AACUnrealGameMode()
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
